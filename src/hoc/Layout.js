import React from "react";
import Auxiliar from "./Auxiliar";
import AppBarButton from "../components/AppBarButton";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { makeStyles } from "@material-ui/core/styles";
import * as ROUTES from "../routes/routes";
import PropTypes from "prop-types";
import { Grid } from "@material-ui/core";
import {
  AccountBox,
  Home,
  FormatListNumberedOutlined,
  Notifications,
  Person,
} from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    maxWidth: "100vw",
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 0px",
    ...theme.mixins.toolbar,
  },
  content: {
    paddingBottom: "10vh",
    paddingTop: "5vh",
    width: "100vw",
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  fixedHeight: {
    height: 240,
  },
  text: {
    padding: theme.spacing(2, 2, 0),
  },
  paper: {
    paddingBottom: 50,
  },
  list: {
    marginBottom: theme.spacing(2),
  },
  subheader: {
    backgroundColor: theme.palette.background.paper,
  },
  appBar: {
    top: "auto",
    bottom: 0,
    backgroundColor: "white",
  },
  toolbar: {
    maxWidth: "100vw",
    display: "flex",
    justifyContent: "center",
  },
}));

const Layout = (props) => {
  const classes = useStyles();

  //   const authContext = React.useContext(AuthContext);

  //   React.useEffect(() => {
  //     authContext.login();
  //   });

  return (
    <Auxiliar>
      <Grid className={classes.root}>
        <CssBaseline />
        <Grid className={classes.content}>{props.children}</Grid>
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar className={classes.toolbar}>
            <AppBarButton
              titleButton="Inicio"
              iconButton={<Home />}
              pathRoute={ROUTES.INICIO}
            />
            <AppBarButton
              titleButton="Candidatos"
              iconButton={<AccountBox />}
              pathRoute={ROUTES.CANDIDATOS}
            />
            <AppBarButton
              titleButton="Elección"
              iconButton={<FormatListNumberedOutlined />}
              pathRoute={ROUTES.ELECCION}
            />
            <AppBarButton
              titleButton="Actividad"
              pathRoute={ROUTES.NOTIFICACIONES}
              iconButton={<Notifications />}
            />
            <AppBarButton
              titleButton="Perfil"
              iconButton={<Person />}
              pathRoute={ROUTES.PERFIL}
            />
          </Toolbar>
        </AppBar>
      </Grid>
    </Auxiliar>
  );
};

Layout.propTypes = {
  children: PropTypes.object,
  TitleLayout: PropTypes.string,
};

export default Layout;
