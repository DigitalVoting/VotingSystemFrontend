import axios from "axios";
import authHeader from "../Authentication/auth-header";
import * as API from "../../services/serverApi";

const API_URL = API.API_URL_ORIGIN + "notification/";

const getNotifications = (userId, candidateListId) => {
  return axios.post(
    API_URL + "list-active",
    {
      userId: userId,
      candidateListId: candidateListId,
    },
    {
      headers: authHeader(),
    }
  );
};

const listCandidatesStatus = (candidateListId) => {
  return axios.post(
    API_URL + "candidates-solicitudes",
    {
      id: candidateListId,
    },
    {
      headers: authHeader(),
    }
  );
};

export default {
  getNotifications,
  listCandidatesStatus,
};
