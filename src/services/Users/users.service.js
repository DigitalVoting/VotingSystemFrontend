import axios from "axios";
import authHeader from "../Authentication/auth-header";
import * as API from "../../services/serverApi";

const API_URL = API.API_URL_ORIGIN + "user/";

const getUser = (id) => {
  return axios.post(
    API_URL + "get",
    {
      userId: id,
      candidateListId: 0,
      candidateInformation: [],
    },
    {
      headers: authHeader(),
    }
  );
};

const getCurrentList = (id) => {
  return axios.post(
    API_URL + "get-current-list",
    {
      id: id,
    },
    {
      headers: authHeader(),
    }
  );
};

const getActiveUsers = () => {
  return axios.post(
    API_URL + "get-active",
    {},
    {
      headers: authHeader(),
    }
  );
};

const findActiveUsers = (find) => {
  return axios.post(
    API_URL + "find-active",
    {
      name: find,
    },
    {
      headers: authHeader(),
    }
  );
};

const findActiveElectors = (find) => {
  return axios.post(
    API_URL + "find-active-electors",
    {
      name: find,
    },
    {
      headers: authHeader(),
    }
  );
};

export default {
  getUser,
  getCurrentList,
  getActiveUsers,
  findActiveUsers,
  findActiveElectors,
};
