import axios from "axios";
import authHeader from "../Authentication/auth-header";
import * as API from "../../services/serverApi";

const API_URL = API.API_URL_ORIGIN + "crypto/";

const getUserPublicKey = (userId) => {
  return axios.post(
    API_URL + "publickey",
    {
      id: userId,
    },
    {
      headers: authHeader(),
    }
  );
};

const doTransaction = (userId, payload, encAesKey) => {
  return axios.post(
    API_URL + "transaction",
    {
      userId,
      payload,
      encAesKey,
    },
    {
      headers: authHeader(),
    }
  );
};

export default {
  getUserPublicKey,
  doTransaction,
};
