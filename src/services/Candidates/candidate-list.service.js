import axios from "axios";
import authHeader from "../Authentication/auth-header";
import * as API from "../../services/serverApi";

const API_URL = API.API_URL_ORIGIN + "candidateslist/";

const getCandidatesLists = (id) => {
  return axios.post(
    API_URL + "listall" + "?id=" + id,
    {},
    {
      headers: authHeader(),
    }
  );
};

const getCandidatesListsCurrent = () => {
  return axios.post(
    API_URL + "listcurrent",
    {},
    {
      headers: authHeader(),
    }
  );
};

const getCandidatesOfCandidatesList = (id) => {
  return axios.post(
    API_URL + "candidates",
    {
      userId: 0,
      candidateListId: id,
      candidateInformation: [],
    },
    {
      headers: authHeader(),
    }
  );
};

const findCandidatesListsCurrent = (find) => {
  return axios.post(
    API_URL + "find",
    {
      name: find,
    },
    {
      headers: authHeader(),
    }
  );
};

const updateCandidateList = (
  candidatesListId,
  listName,
  description,
  logoUrl,
  status,
  updateUser,
  type
) => {
  return axios.post(
    API_URL + "update",
    {
      candidatesListId: candidatesListId,
      name: listName,
      description: description,
      logoUrl: logoUrl,
      status: status,
      updateUser: updateUser,
      type: type,
    },
    {
      headers: authHeader(),
    }
  );
};

const saveCandidatesList = (
  candidatesList,
  candidates,
  userId,
  status,
  electionId
) => {
  return axios.post(
    API_URL + "save",
    {
      name: candidatesList.name,
      description: candidatesList.description,
      status: status,
      logoUrl: candidatesList.logo,
      creationUser: userId,
      users: candidates,
      election: electionId,
    },
    {
      headers: authHeader(),
    }
  );
};

const answerSolicitude = (userId, candidatesListId, answer, notificationId) => {
  return axios.post(
    API_URL + "answer-solicitude",
    {
      userId,
      candidatesListId,
      answer,
      notificationId,
    },
    {
      headers: authHeader(),
    }
  );
};

export default {
  getCandidatesLists,
  getCandidatesListsCurrent,
  getCandidatesOfCandidatesList,
  findCandidatesListsCurrent,
  updateCandidateList,
  saveCandidatesList,
  answerSolicitude,
};
