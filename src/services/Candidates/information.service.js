import axios from "axios";
import authHeader from "../Authentication/auth-header";
import * as API from "../../services/serverApi";

const API_URL = API.API_URL_ORIGIN + "information/";

const getCandidateInformation = (candidateId, listId, status) => {
  return axios.post(
    API_URL + "candidate",
    {
      userId: candidateId,
      candidateListId: listId,
      candidateInformation: [],
      status: status,
    },
    {
      headers: authHeader(),
    }
  );
};

const saveCandidateInformation = (
  candidateId,
  listId,
  status,
  candidateInformation
) => {
  return axios.post(
    API_URL + "saveInformation",
    {
      userId: candidateId,
      candidateListId: listId,
      status: status,
      candidateInformation: candidateInformation,
    },
    {
      headers: authHeader(),
    }
  );
};

export default {
  getCandidateInformation,
  saveCandidateInformation,
};
