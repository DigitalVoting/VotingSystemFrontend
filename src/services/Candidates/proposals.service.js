import axios from "axios";
import authHeader from "../Authentication/auth-header";
import * as API from "../../services/serverApi";

const API_URL = API.API_URL_ORIGIN + "proposal/";

const updateProposals = (candidateListId, creationUserId, proposals) => {
  return axios.post(
    API_URL + "update",
    {
      candidateListId: candidateListId,
      creationUserId: creationUserId,
      proposals: proposals,
    },
    {
      headers: authHeader(),
    }
  );
};

export default {
  updateProposals,
};
