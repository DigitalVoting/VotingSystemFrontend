import axios from "axios";
import authHeader from "../Authentication/auth-header";
import * as API from "../../services/serverApi";

const API_URL = API.API_URL_ORIGIN + "user/candidates/";

const getCandidatesCurrent = () => {
  return axios.post(
    API_URL + "listcurrent",
    {},
    {
      headers: authHeader(),
    }
  );
};

const findCandidatesCurrent = (find) => {
  return axios.post(
    API_URL + "find",
    {
      name: find,
    },
    {
      headers: authHeader(),
    }
  );
};

export default {
  getCandidatesCurrent,
  findCandidatesCurrent,
};
