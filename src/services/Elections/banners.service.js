import axios from "axios";
import authHeader from "../Authentication/auth-header";
import * as API from "../../services/serverApi";

const API_URL = API.API_URL_ORIGIN + "banner/";

const getBanners = () => {
  return axios.post(
    API_URL + "list-active",
    {},
    {
      headers: authHeader(),
    }
  );
};

export default {
  getBanners,
};
