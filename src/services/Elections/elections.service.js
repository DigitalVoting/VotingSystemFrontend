import axios from "axios";
import authHeader from "../Authentication/auth-header";
import * as API from "../../services/serverApi";

const API_URL = API.API_URL_ORIGIN + "election/";

const activeElection = () => {
  return axios.post(
    API_URL + "active-election",
    {},
    {
      headers: authHeader(),
    }
  );
};

const getStage = () => {
  return axios.post(
    API_URL + "active-election-stage",
    {},
    {
      headers: authHeader(),
    }
  );
};

const generateToken = (userId, electionId) => {
  return axios.post(
    API_URL + "generate-token",
    {
      userId,
      electionId,
    },
    {
      headers: authHeader(),
    }
  );
};

const hasVoted = (userId, electionId) => {
  return axios.post(
    API_URL + "has-voted",
    {
      userId,
      electionId,
    },
    {
      headers: authHeader(),
    }
  );
};

const confirmToken = (userId, electionId, token) => {
  return axios.post(
    API_URL + "confirm-token",
    {
      userId,
      electionId,
      token,
    },
    {
      headers: authHeader(),
    }
  );
};

const getResults = (electionId) => {
  return axios.post(
    API_URL + "results",
    {
      id: electionId,
    },
    {
      headers: authHeader(),
    }
  );
};

export default {
  activeElection,
  getStage,
  generateToken,
  hasVoted,
  confirmToken,
  getResults,
};
