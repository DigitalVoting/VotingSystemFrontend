import axios from "axios";
import * as API from "../../services/serverApi";

const API_URL = API.API_URL_ORIGIN + "auth/";

const register = (username, names, lastNames, creationUser, role, password) => {
  return axios.post(API_URL + "signup", {
    username,
    names,
    lastNames,
    creationUser,
    role,
    password,
  });
};

const signIn = (username, password) => {
  return axios
    .post(API_URL + "signin", {
      username,
      password,
    })
    .then((response) => {
      if (response.data.accessToken) {
        localStorage.setItem("user", JSON.stringify(response.data));
        localStorage.setItem("userAuth", true);
      }

      return response.data;
    });
};

const signOut = () => {
  localStorage.setItem("userAuth", false);
  localStorage.removeItem("userAuth");
  localStorage.removeItem("user");
};

const restorePassword = (username) => {
  return axios
    .post(API_URL + "restore-password", {
      username,
      password: "password",
    })
    .then((response) => {
      return response.data;
    });
};

const confirmToken = (userId, token) => {
  return axios
    .post(API_URL + "confirm-token", {
      userId,
      password: "password",
      token,
    })
    .then((response) => {
      return response.data;
    });
};

const changePassword = (userId, password, token) => {
  return axios
    .post(API_URL + "change-password", {
      userId,
      password,
      token,
    })
    .then((response) => {
      return response.data;
    });
};

export default {
  register,
  signIn,
  signOut,
  restorePassword,
  confirmToken,
  changePassword,
};
