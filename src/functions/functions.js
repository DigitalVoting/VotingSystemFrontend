function pad(number) {
  if (number < 10) {
    return "0" + number;
  }
  return number;
}
export function convertToFormatIso(dateTime) {
  return (
    dateTime.getFullYear() +
    "-" +
    pad(dateTime.getMonth() + 1) +
    "-" +
    pad(dateTime.getDate()) +
    "T" +
    pad(dateTime.getHours()) +
    ":" +
    pad(dateTime.getMinutes()) +
    ":" +
    pad(dateTime.getSeconds())
  );
}
export function convertToFormatBackend(dateTime) {
  return (
    dateTime.getFullYear() +
    "-" +
    pad(dateTime.getMonth() + 1) +
    "-" +
    pad(dateTime.getDate())
  );
}
export function getTime(dateStart, dateEnd) {
  dateStart.setHours(0);
  dateStart.setMinutes(0);
  dateStart.setSeconds(0);
  dateEnd.setHours(23);
  dateEnd.setMinutes(59);
  dateEnd.setSeconds(59);
  return parseInt(
    Math.ceil((dateEnd.getTime() - dateStart.getTime()) / 86400000)
  );
}
export function obtainFullname(name, lastname) {
  const indName = name.indexOf(" ");
  const indLastname = lastname.indexOf(" ");
  const namePerson = indName < 0 ? name : name.slice(0, indName);
  const lastnamePerson =
    indLastname < 0 ? lastname : lastname.slice(0, indLastname);
  return namePerson + " " + lastnamePerson;
}
