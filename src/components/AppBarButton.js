import React from "react";
import { Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { useHistory, useLocation } from "react-router";
import theme from "../assets/theme/theme";

const useStyles = makeStyles(() => ({
  button: {
    padding: "0.8vw",
  },
  label: {
    // Aligns the content of the button vertically.
    flexDirection: "column",
    fontSize: "10px !important",
  },
  icon: {
    fontSize: "24px !important",
    padding: "0vh",
  },
}));

const AppBarButton = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const location = useLocation();

  return (
    <Button
      /* Use classes property to inject custom styles */
      classes={{ root: classes.button, label: classes.label }}
      variant="text"
      style={{
        color:
          props.pathRoute === location.pathname
            ? theme.palette.secondary.main
            : theme.palette.secondary.dark,
      }}
      disableRipple={true}
      onClick={() => {
        history.push(props.pathRoute);
      }}
    >
      {props.iconButton}
      {props.titleButton}
    </Button>
  );
};

AppBarButton.propTypes = {
  iconButton: PropTypes.object,
  titleButton: PropTypes.string,
  pathRoute: PropTypes.string,
};

export default AppBarButton;
