import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";
import { CardContent, Grid } from "@material-ui/core";
import { useHistory } from "react-router";
import * as ROUTES from "../routes/routes";

const useStyles = makeStyles((theme) => ({
  root: {
    border: "1px solid",
    borderColor: theme.palette.secondary.main,
    borderRadius: "12px",
    margin: theme.spacing(1),
    cursor: "pointer",
  },
  content: {
    background: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
    padding: theme.spacing(1),
  },
  name: {
    cursor: "pointer",
  },
}));

const ListCard = (props) => {
  const classes = useStyles();
  const history = useHistory();

  const listDetails = () => {
    history.push(ROUTES.LISTA_DETALLES);
    localStorage.setItem("details", JSON.stringify(props.list));
  };

  return (
    <Grid container justify="center">
      <Grid
        item
        container
        direction="column"
        align="center"
        justify="center"
        onClick={listDetails.bind(this)}
      >
        <Card className={classes.root} key={props.keyID}>
          <CardContent className={classes.content}>
            <img
              alt={props.imageTitle}
              src={props.imageList}
              style={{ width: "100px", height: "100px", background: "white" }}
            />
          </CardContent>
        </Card>

        <Typography variant="h6" className={classes.name}>
          {props.titleList}
        </Typography>
      </Grid>
    </Grid>
  );
};

ListCard.propTypes = {
  keyID: PropTypes.number,
  titleList: PropTypes.string,
  imageList: PropTypes.string,
  imageTitle: PropTypes.string,
  list: PropTypes.object,
};
export default ListCard;
