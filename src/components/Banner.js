import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 325,
    maxWidth: 325,
    border: "1px solid",
    borderColor: theme.palette.secondary.main,
    borderRadius: "12px",
  },
  media: {
    height: 0,
    padding: theme.spacing(18),
  },
  header: {
    background: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
  },
}));

const CardBanner = (props) => {
  const classes = useStyles();
  return (
    <Card className={classes.root} key={props.keyID}>
      <CardMedia
        className={classes.media}
        image={props.imageBanner}
        title={props.imageTitle}
      />
      <CardHeader
        disableTypography
        title={<Typography variant="h6">{props.titleBanner}</Typography>}
        className={classes.header}
      />
    </Card>
  );
};

CardBanner.propTypes = {
  keyID: PropTypes.number,
  titleBanner: PropTypes.string,
  imageBanner: PropTypes.string,
  imageTitle: PropTypes.string,
};
export default CardBanner;
