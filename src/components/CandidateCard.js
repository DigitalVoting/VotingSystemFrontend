import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import PropTypes from "prop-types";
import { Avatar, CardContent, Grid } from "@material-ui/core";
import { useHistory } from "react-router";
import * as ROUTES from "../routes/routes";

const useStyles = makeStyles((theme) => ({
  root: {
    border: "0px solid",
    borderColor: theme.palette.secondary.main,
    borderRadius: "12px",
    boxShadow: "none",
    margin: theme.spacing(0),
    cursor: "pointer",
  },
  header: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    cursor: "pointer",
  },
  content: {
    background: "#fafafa",
    color: theme.palette.secondary.contrastText,
    padding: theme.spacing(0),
    paddingBottom: "0px !important",
  },
  avatar: {
    width: theme.spacing(17),
    height: theme.spacing(17),
  },
  lista: {
    width: theme.spacing(5),
    height: theme.spacing(5),
  },
}));

const CandidateCard = (props) => {
  const classes = useStyles();
  const history = useHistory();

  const candidateDetails = () => {
    history.push(ROUTES.CANDIDATO_DETALLES);
    localStorage.setItem("details", JSON.stringify(props.candidate));
  };

  const listDetails = () => {
    history.push(ROUTES.LISTA_DETALLES);
    localStorage.setItem(
      "details",
      JSON.stringify(props.candidate.candidatesLists[0])
    );
  };

  return (
    <Grid container justify="center">
      <Grid item container direction="column" align="center" justify="center">
        <Grid
          item
          container
          direction="row"
          align="center"
          className={classes.header}
          onClick={listDetails.bind(this)}
        >
          <Grid item xs={4}>
            <Avatar
              alt={props.listLogoDescription}
              src={props.listLogo}
              className={classes.lista}
            />
          </Grid>
          <Grid item xs={8}>
            <Typography
              variant="body2"
              align="left"
              style={{ color: "#3E5481", alignItems: "center" }}
            >
              {props.list}
            </Typography>
          </Grid>
        </Grid>
        <Card
          className={classes.root}
          key={props.keyID}
          onClick={candidateDetails.bind(this)}
        >
          <CardContent className={classes.content}>
            <Avatar
              alt={props.description}
              src={props.image}
              className={classes.avatar}
            />
          </CardContent>
        </Card>

        <Typography
          variant="subtitle1"
          align="left"
          style={{ color: "#3E5481", cursor: "pointer" }}
          onClick={candidateDetails.bind(this)}
        >
          {props.name}
        </Typography>
        <Typography
          variant="subtitle2"
          align="left"
          style={{ color: "#9FA5C0", cursor: "pointer" }}
          onClick={candidateDetails.bind(this)}
        >
          {props.role}
        </Typography>
      </Grid>
    </Grid>
  );
};

CandidateCard.propTypes = {
  keyID: PropTypes.number,
  name: PropTypes.string,
  list: PropTypes.string,
  role: PropTypes.string,
  image: PropTypes.string,
  description: PropTypes.string,
  listLogo: PropTypes.string,
  listLogoDescription: PropTypes.string,
  candidate: PropTypes.object,
};
export default CandidateCard;
