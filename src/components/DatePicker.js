import React, { Fragment } from "react";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import es_PELocale from "date-fns/locale/es";
import DateFnsUtils from "@date-io/date-fns";
import { makeStyles } from "@material-ui/core/styles";

import PropTypes from "prop-types";
const useStyles = makeStyles(() => ({
  root: {},
}));

function DatePicker(props) {
  const classes = useStyles();
  return (
    <Fragment>
      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={es_PELocale}>
        <KeyboardDatePicker
          className={classes.root}
          label={props.label}
          cancelLabel="Cancelar"
          readOnly={props.readOnly}
          variant="dialog"
          inputVariant="outlined"
          placeholder={props.label}
          value={props.selectedDate}
          onChange={(date) => props.handleDateChange(date)}
          format="dd/MM/yyyy"
          minDate={props.minDate}
          minDateMessage="La fecha debe ser mayor a su dia minimo"
        />
      </MuiPickersUtilsProvider>
    </Fragment>
  );
}
DatePicker.propTypes = {
  selectedDate: PropTypes.object,
  handleDateChange: PropTypes.func,
  label: PropTypes.string,
  readOnly: PropTypes.bool,
  minDate: PropTypes.object,
};
export default DatePicker;
