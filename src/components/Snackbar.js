import React from "react";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";

function AlertMod(props) {
  return <Alert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));

const SnackbarComponent = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Snackbar
        open={props.open}
        autoHideDuration={6000}
        onClose={props.handleClose}
        anchorOrigin={{ vertical: "top", horizontal: "center" }}
      >
        <AlertMod onClose={props.handleClose} severity={props.severity}>
          {props.message}
        </AlertMod>
      </Snackbar>
    </div>
  );
};

SnackbarComponent.propTypes = {
  message: PropTypes.string,
  handleClose: PropTypes.func,
  severity: PropTypes.string,
  open: PropTypes.bool,
};
export default SnackbarComponent;
