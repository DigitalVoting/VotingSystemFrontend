import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#94F5D3",
      main: "#D1B059",
      dark: "#D1B059",
      contrastText: "#FFFFFF",
    },
    secondary: {
      light: "#528EA9",
      main: "#851610",
      dark: "#9FA5C0",
      contrastText: "#FFFFFF",
    },
    error: {
      main: "#CF433A",
    },
  },
  typography: {
    fontFamily: ["Inter", "sans-serif"].join(","),
  },
  overrides: {
    MuiButton: {
      root: {
        borderRadius: 18,
      },
    },
    MuiOutlinedInput: {
      root: {
        borderRadius: 24,
      },
    },
    MuiFilledInput: {
      underline: { color: "white" },
    },
  },
});

export default theme;
