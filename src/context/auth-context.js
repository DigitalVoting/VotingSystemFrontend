import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

export const AuthContext = React.createContext({
  isAuth: false,
  login: () => {},
});

const AuthContextProvider = (props) => {
  const [isAuthenticated, setIsAuthenticated] = useState(
    localStorage.getItem("userAuth") || false
  );
  const loginHandler = () => {
    setIsAuthenticated(true);
    localStorage.setItem("Fecha", JSON.stringify({ fecha: Date.now() }));
  };
  useEffect(() => {
    setIsAuthenticated(localStorage.getItem("userAuth"));
  }, [isAuthenticated]);
  const signoutHandler = () => {
    setIsAuthenticated(false);
  };

  return (
    <AuthContext.Provider
      value={{
        login: loginHandler,
        signout: signoutHandler,
        isAuth: isAuthenticated,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

AuthContextProvider.propTypes = {
  children: PropTypes.object,
};

export default AuthContextProvider;
