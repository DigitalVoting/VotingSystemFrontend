import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import userService from "../../services/Users/users.service";
import notificationsService from "../../services/Users/notifications.service";
import {
  Avatar,
  Button,
  Card,
  CardContent,
  Fade,
  Modal,
  Typography,
} from "@material-ui/core";
import Backdrop from "@material-ui/core/Backdrop";
import candidateListService from "../../services/Candidates/candidate-list.service";
import * as ROUTES from "../../routes/routes";
import { useHistory } from "react-router";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: "100vw",
  },
  titleText: {
    margin: theme.spacing(1),
    marginTop: -theme.spacing(1),
    color: "#D1B059",
  },
  notification: {
    cursor: "pointer",
    padding: "0",
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: "10px",
    maxWidth: "90vw",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 2, 3),
  },
  cover: {
    width: 151,
  },
}));

const Notifications = () => {
  const classes = useStyles();
  const history = useHistory();
  const user = JSON.parse(localStorage.getItem("user"));

  const [notifications, setNotifications] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [informationModal, setInformationModal] = useState();

  const [online, setOnline] = useState(navigator.onLine);

  window.addEventListener("offline", function () {
    if (online) {
      setOnline(false);
      localStorage.setItem("notifications", JSON.stringify(notifications));
    }
  });

  window.addEventListener("online", function () {
    if (!online) {
      localStorage.removeItem("notifications");
      setOnline(true);
      GetNotifications();
    }
  });

  const GetNotifications = () => {
    if (user.roles.includes("ROLE_LIST_ADMIN")) {
      userService
        .getCurrentList(user.id)
        .then((response) => {
          if (response.data) {
            notificationsService
              .getNotifications(user.id, response.data.id)
              .then((noti) => {
                if (noti.data) {
                  let aux = [];
                  for (const not of noti.data) {
                    if (not.title === "Solicitud de Miembro de Lista") {
                      if (not.user.id === user.id) aux.push(not);
                    } else aux.push(not);
                  }
                  setNotifications(aux);
                }
              })
              .catch((e) => {
                console.log(e);
              });
          }
        })
        .catch((e) => {
          console.log(e);
        });
    } else {
      notificationsService
        .getNotifications(user.id, 0)
        .then((noti) => {
          if (noti.data) {
            let aux = [];
            for (const not of noti.data) {
              if (
                not.title !== "Solicitud Aceptada" &&
                not.title !== "Solicitud Rechazada"
              )
                aux.push(not);
            }
            setNotifications(aux);
          }
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };

  useEffect(() => {
    const notificationsSaved = JSON.parse(
      localStorage.getItem("notifications")
    );
    if (navigator.onLine) {
      GetNotifications();
      if (notificationsSaved) {
        localStorage.removeItem("notifications");
      }
    } else {
      if (notificationsSaved) {
        setNotifications(notificationsSaved);
      }
    }
  }, []);

  const NotificationAction = (notification) => {
    if (notification.title === "Solicitud de Miembro de Lista") {
      setInformationModal(notification);
      setOpen(true);
    } else if (notification.title === "Actualización de Información") {
      history.push(ROUTES.LISTA_DETALLES);
      localStorage.setItem(
        "details",
        JSON.stringify(notification.candidatesList)
      );
    }
  };

  const handleClose = () => {
    setOpen(false);
  };

  const Responder = (accepted) => {
    candidateListService
      .answerSolicitude(
        user.id,
        informationModal.candidatesList.id,
        accepted,
        informationModal.id
      )
      .then((response) => {
        if (response.data) {
          let notAux = [];

          for (const not of notifications) {
            if (not.id != informationModal.id) notAux.push(not);
          }

          setNotifications(notAux);
        }
      })
      .catch((e) => {
        console.log(e);
      });

    handleClose();
  };

  return (
    <Grid container justify="center">
      <Grid
        item
        container
        direction="column"
        justify="center"
        align="center"
        className={classes.titleText}
      >
        <Typography variant="h4" align="left">
          Actividad
        </Typography>
      </Grid>
      <Grid
        container
        direction="column"
        justify="center"
        align="center"
        className={classes.root}
        spacing={3}
      >
        {notifications.map((notification) => {
          return (
            <Grid
              key={notification.id}
              item
              onClick={NotificationAction.bind(this, notification)}
              className={classes.notification}
            >
              <Card className={classes.root}>
                <Grid container direction="row">
                  <Grid
                    item
                    xs={2}
                    container
                    justify="center"
                    align="center"
                    direction="column"
                  >
                    <Grid item>
                      <Avatar
                        alt={notification.title}
                        src={
                          notification.user
                            ? notification.user.photo
                            : notification.candidatesList.logo
                        }
                        className={classes.lista}
                      />
                    </Grid>
                  </Grid>
                  <Grid xs={10} item container direction="column">
                    <CardContent className={classes.content}>
                      <Typography variant="h6" align="left">
                        {notification.title}
                      </Typography>
                      <Typography variant="subtitle2" align="justify">
                        {notification.description}
                      </Typography>
                    </CardContent>
                  </Grid>
                </Grid>
              </Card>
            </Grid>
          );
        })}
        {notifications[0] ? (
          <Grid></Grid>
        ) : (
          <Grid item className={classes.notification}>
            <Card className={classes.root}>
              <Grid container direction="row">
                <Grid
                  item
                  xs={2}
                  container
                  justify="center"
                  align="center"
                  direction="column"
                >
                  <Grid item>
                    <Avatar
                      alt={"Registros Nulos"}
                      src={null}
                      className={classes.lista}
                    />
                  </Grid>
                </Grid>
                <Grid xs={10} item container direction="column">
                  <CardContent className={classes.content}>
                    <Typography variant="h6" align="left">
                      {"No se cuenta con registros disponibles"}
                    </Typography>
                  </CardContent>
                </Grid>
              </Grid>
            </Card>
          </Grid>
        )}
      </Grid>
      <Modal
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        {informationModal ? (
          <Fade in={open}>
            <Grid container className={classes.paper}>
              <Grid item container style={{ margin: "1vh" }}>
                <Grid item container style={{ alignContent: "center" }}>
                  <Typography
                    variant="h6"
                    className={classes.titleText}
                    align="center"
                  >
                    {"¿Acepta pertenecer a la lista " +
                      informationModal.candidatesList.name +
                      "?"}
                  </Typography>
                </Grid>
              </Grid>
              <Grid container item justify="center">
                <Grid
                  container
                  item
                  xs={5}
                  sm={4}
                  justify="center"
                  style={{
                    paddingBottom: "1vh",
                    paddingTop: "1vh",
                    margin: "1vh",
                  }}
                >
                  <Button
                    border={15}
                    variant="contained"
                    fullWidth
                    size="large"
                    color="primary"
                    className={classes.button}
                    onClick={Responder.bind(this, true)}
                  >
                    Aceptar
                  </Button>
                </Grid>
                <Grid
                  container
                  item
                  xs={5}
                  sm={4}
                  justify="center"
                  style={{
                    paddingBottom: "1vh",
                    paddingTop: "1vh",
                    margin: "1vh",
                  }}
                >
                  <Button
                    border={15}
                    variant="contained"
                    fullWidth
                    size="large"
                    color="secondary"
                    className={classes.button}
                    onClick={Responder.bind(this, false)}
                  >
                    Rechazar
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Fade>
        ) : (
          <Grid></Grid>
        )}
      </Modal>
    </Grid>
  );
};
export default Notifications;
