import {
  Avatar,
  Button,
  Grid,
  makeStyles,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import authService from "../../services/Authentication/auth.service";
import userService from "../../services/Users/users.service";
import electionsService from "../../services/Elections/elections.service";
import { AuthContext } from "../../context/auth-context";
import { useHistory } from "react-router";
import * as ROUTES from "../../routes/routes";
import Snackbar from "../../components/Snackbar";

const useStyles = makeStyles((theme) => ({
  title: {},
  titleText: {
    color: "#2E3E5C",
  },
  button: {},
  content: {
    minHeight: "85vh",
    maxWidth: "100vw",
    marginLeft: "0px",
  },
  avatar: {
    width: theme.spacing(15),
    height: theme.spacing(15),
  },
}));

const Profile = () => {
  const classes = useStyles();
  const history = useHistory();

  const [profile, setProfile] = useState();
  const [stage, setStage] = useState("results");

  const user = JSON.parse(localStorage.getItem("user"));
  const [error, setError] = React.useState(false);
  const [errorText, setErrorText] = useState("");

  const authContext = React.useContext(AuthContext);

  const signOut = () => {
    authService.signOut();
    authContext.signout();
  };

  const changePassword = () => {
    if (navigator.onLine) {
      history.push(ROUTES.RECUPERAR_CONTRASENA);
    } else {
      setErrorText("No cuenta con conexión a internet");
      setError(true);
    }
  };

  const candidateProfile = () => {
    if (navigator.onLine) {
      history.push(ROUTES.PERFIL_CANDIDATO);
    } else {
      setErrorText("No cuenta con conexión a internet");
      setError(true);
    }
  };

  const listProfile = () => {
    if (navigator.onLine) {
      history.push(ROUTES.PERFIL_LISTA_CANDIDATO);
    } else {
      setErrorText("No cuenta con conexión a internet");
      setError(true);
    }
  };

  const handleCloseError = () => {
    setError(false);
  };

  useEffect(() => {
    userService
      .getUser(user.id)
      .then((response) => {
        if (response.data) {
          setProfile(response.data);
        }
      })
      .catch((e) => {
        console.log(e);
      });
    electionsService
      .getStage(user.id)
      .then((response) => {
        if (response.data) {
          setStage(response.data);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  return (
    <Grid
      container
      direction="column"
      justify="center"
      align="center"
      className={classes.content}
      spacing={2}
    >
      <Grid item container justify="center">
        <Avatar
          alt={user.names}
          src={profile ? profile.photo : user.photo}
          className={classes.avatar}
        />
      </Grid>
      <Grid item container justify="center" className={classes.title}>
        <Typography variant="h5" className={classes.titleText}>
          {user.names + " " + user.lastNames}
        </Typography>
      </Grid>
      {user.roles.includes("ROLE_CANDIDATE") &&
      user.roles.includes("ROLE_LIST_ADMIN") &&
      stage == "actualization" ? (
        <Grid item container justify="center">
          <Grid
            item
            container
            xs={10}
            sm={4}
            justify="center"
            style={{ paddingTop: "3vh" }}
          >
            <Button
              border={15}
              variant="contained"
              fullWidth
              size="large"
              color="primary"
              className={classes.button}
              onClick={listProfile.bind(this)}
            >
              Lista de Candidatos
            </Button>
          </Grid>
        </Grid>
      ) : null}
      {user.roles.includes("ROLE_CANDIDATE") && stage == "actualization" ? (
        <Grid item container justify="center">
          <Grid
            item
            container
            xs={10}
            sm={4}
            justify="center"
            style={{ paddingTop: "3vh" }}
          >
            <Button
              border={15}
              variant="contained"
              fullWidth
              size="large"
              color="secondary"
              className={classes.button}
              onClick={candidateProfile.bind(this)}
            >
              Perfil Candidato
            </Button>
          </Grid>
        </Grid>
      ) : null}
      <Grid item container justify="center">
        <Grid
          item
          container
          xs={10}
          sm={4}
          justify="center"
          style={{ paddingTop: "3vh" }}
        >
          <Button
            border={15}
            variant="contained"
            fullWidth
            size="large"
            color="primary"
            className={classes.button}
            onClick={changePassword.bind(this)}
          >
            Cambiar Contraseña
          </Button>
        </Grid>
      </Grid>
      <Grid item container justify="center">
        <Grid
          item
          container
          xs={10}
          sm={4}
          justify="center"
          style={{ paddingTop: "3vh" }}
        >
          <Button
            border={15}
            variant="contained"
            fullWidth
            size="large"
            color="secondary"
            className={classes.button}
            onClick={signOut.bind(this)}
          >
            Cerrar Sesión
          </Button>
        </Grid>
      </Grid>
      <Grid container item xs={10} justify="flex-end">
        <Snackbar
          message={errorText}
          handleClose={handleCloseError}
          open={error}
          severity="error"
        />
      </Grid>
    </Grid>
  );
};

export default Profile;
