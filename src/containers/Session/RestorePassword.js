import {
  Button,
  Grid,
  InputAdornment,
  makeStyles,
  TextField,
  Typography,
} from "@material-ui/core";
import { PersonOutline } from "@material-ui/icons";
import React, { useState } from "react";
import { useHistory } from "react-router";
import * as ROUTES from "../../routes/routes";
import authService from "../../services/Authentication/auth.service";
import Snackbar from "../../components/Snackbar";

const useStyles = makeStyles(() => ({
  title: {},
  titleText: {
    color: "#2E3E5C",
  },
  subtitle: {
    paddingBottom: "5vh",
  },
  subtitleText: {
    color: "#9FA5C0",
  },
  button: {},
  content: {
    minHeight: "100vh",
  },
}));

const RestorePassword = () => {
  const classes = useStyles();
  const history = useHistory();

  const [username, setUserName] = useState("");
  const [open, setOpen] = React.useState(false);

  function Restore() {
    authService
      .restorePassword(username)
      .then((response) => {
        history.push({
          pathname: ROUTES.CONFIRMAR_TOKEN,
          state: { id: response, username: username },
        });
      })
      .catch((e) => {
        console.log(e);
        setOpen(true);
      });
  }

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  return (
    <Grid
      container
      direction="column"
      justify="center"
      align="center"
      className={classes.content}
    >
      <Grid item container justify="center" className={classes.title}>
        <Typography variant="h5" className={classes.titleText}>
          Recuperación de Contraseña
        </Typography>
      </Grid>
      <Grid item container justify="center" className={classes.subtitle}>
        <Typography variant="subtitle1" className={classes.subtitleText}>
          Ingresa tu usuario para la recuperación
        </Typography>
      </Grid>
      <Grid item container justify="center">
        <Grid
          container
          direction="row"
          item
          xs={10}
          sm={6}
          justify="center"
          style={{ paddingBottom: "3vh" }}
        >
          <TextField
            fullWidth
            value={username}
            placeholder="Usuario"
            onChange={(event) => {
              setUserName(event.target.value);
            }}
            variant="outlined"
            type="text"
            size="small"
            autoComplete="current-user"
            inputProps={{ maxLength: 20 }}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <PersonOutline />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
      </Grid>
      <Grid item container justify="center">
        <Grid
          container
          item
          xs={10}
          sm={6}
          justify="center"
          style={{ paddingBottom: "3vh", paddingTop: "3vh" }}
        >
          <Button
            border={15}
            variant="contained"
            fullWidth
            size="large"
            color="primary"
            className={classes.button}
            onClick={Restore.bind(this)}
          >
            Ingresar
          </Button>
        </Grid>
      </Grid>
      <Grid container item xs={10} justify="flex-end">
        <Snackbar
          message="El usuario ingresado no existe"
          handleClose={handleClose}
          open={open}
          severity="error"
        />
      </Grid>
    </Grid>
  );
};

export default RestorePassword;
