import {
  Button,
  Grid,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from "@material-ui/core";
import {
  LockOutlined,
  PersonOutline,
  Visibility,
  VisibilityOff,
} from "@material-ui/icons";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import logo from "../../assets/img/Logo.png";
import * as ROUTES from "../../routes/routes";
import authService from "../../services/Authentication/auth.service";
import { AuthContext } from "../../context/auth-context";

import Snackbar from "../../components/Snackbar";

const Login = () => {
  const authContext = React.useContext(AuthContext);

  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const handleMouseDownPassword = () => setShowPassword(!showPassword);
  const [open, setOpen] = React.useState(false);

  function onKeyPress(event) {
    const keyCode = event.keyCode || event.which;
    const keyValue = String.fromCharCode(keyCode);
    const regex = /^[a-zA-Z\d\w\W]+$/;
    if (!regex.test(keyValue)) event.preventDefault();
  }

  const logIn = () => {
    authService
      .signIn(user, password)
      .then((response) => {
        if (response.id) {
          authContext.login();
        }
      })
      .catch((e) => {
        console.log(e);
        setOpen(true);
      });
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  return (
    <Grid container justify="center">
      <Grid container item justify="center" md={6} style={{ padding: "8vh" }}>
        <img alt="Logo" src={logo} style={{ width: "70%" }} />
      </Grid>
      <Grid container justify="center">
        <Typography variant="h5" style={{ color: "#2E3E5C" }}>
          Bienvenido
        </Typography>
      </Grid>
      <Grid container justify="center" style={{ paddingBottom: "5vh" }}>
        <Typography variant="subtitle1" style={{ color: "#9FA5C0" }}>
          Por favor ingresa tus credenciales aquí
        </Typography>
      </Grid>
      <Grid
        container
        direction="row"
        item
        xs={10}
        justify="center"
        style={{ paddingBottom: "3vh" }}
      >
        <TextField
          fullWidth
          value={user}
          placeholder="Usuario"
          onChange={(event) => {
            setUser(event.target.value);
          }}
          variant="outlined"
          type="text"
          size="small"
          autoComplete="current-user"
          inputProps={{ maxLength: 20 }}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <PersonOutline />
              </InputAdornment>
            ),
          }}
        />
      </Grid>
      <Grid
        container
        direction="row"
        item
        xs={10}
        justify="center"
        style={{ paddingBottom: "3vh" }}
      >
        <TextField
          fullWidth
          value={password}
          placeholder="Contraseña"
          onChange={(event) => {
            setPassword(event.target.value);
          }}
          variant="outlined"
          type={showPassword ? "text" : "password"}
          size="small"
          autoComplete="current-password"
          onKeyPress={onKeyPress.bind(this)}
          inputProps={{ maxLength: 20 }}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onMouseDown={handleMouseDownPassword}
                >
                  {showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
            startAdornment: (
              <InputAdornment position="start">
                <LockOutlined />
              </InputAdornment>
            ),
          }}
        />
      </Grid>
      <Grid container item xs={10} justify="flex-end">
        <Link
          to={ROUTES.RECUPERAR_CONTRASENA}
          style={{ textDecoration: "none" }}
        >
          <Typography variant="subtitle1" style={{ color: "#2E3E5C" }}>
            ¿Olvidaste tu contraseña?
          </Typography>
        </Link>
      </Grid>
      <Grid
        container
        item
        xs={10}
        sm={4}
        justify="center"
        style={{ paddingBottom: "3vh", paddingTop: "3vh" }}
      >
        <Button
          border={15}
          variant="contained"
          fullWidth
          size="large"
          color="primary"
          onClick={logIn.bind(this)}
        >
          Ingresar
        </Button>
      </Grid>
      <Grid container item xs={10} justify="flex-end">
        <Snackbar
          message="El usuario o contraseña ingresado es incorrecto"
          handleClose={handleClose}
          open={open}
          severity="error"
        />
      </Grid>
    </Grid>
  );
};

export default Login;
