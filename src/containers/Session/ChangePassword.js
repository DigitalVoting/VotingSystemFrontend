import {
  Button,
  Grid,
  IconButton,
  InputAdornment,
  makeStyles,
  TextField,
  Typography,
} from "@material-ui/core";
import { LockOutlined, Visibility, VisibilityOff } from "@material-ui/icons";
import React, { useState } from "react";
import { useEffect } from "react";
import { useLocation } from "react-router-dom";
import { useHistory } from "react-router";
import * as ROUTES from "../../routes/routes";
import authService from "../../services/Authentication/auth.service";
import { AuthContext } from "../../context/auth-context";
import Snackbar from "../../components/Snackbar";

const useStyles = makeStyles(() => ({
  title: {},
  titleText: {
    color: "#2E3E5C",
  },
  subtitle: {
    paddingBottom: "5vh",
  },
  subtitleText: {
    color: "#9FA5C0",
  },
  button: {},
  content: {
    minHeight: "100vh",
  },
}));

const ChangePassword = () => {
  const classes = useStyles();
  const location = useLocation();
  const history = useHistory();
  const authContext = React.useContext(AuthContext);

  const [password, setPassword] = useState("");
  const [open, setOpen] = React.useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const handleMouseDownPassword = () => setShowPassword(!showPassword);

  function Change() {
    let regex = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
    if (!regex.test(password)) {
      setOpen(true);
    } else {
      authService
        .changePassword(location.state.id, password, location.state.token)
        .then((response) => {
          if (response != 1) {
            setOpen(true);
          } else {
            authService
              .signIn(location.state.username, password)
              .then((responseLogin) => {
                if (responseLogin.id) {
                  authContext.login();
                }
              })
              .catch((e) => {
                console.log(e);
                setOpen(true);
              });
          }
        })
        .catch((e) => {
          console.log(e);
          setOpen(true);
        });
    }
  }

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  function onKeyPress(event) {
    const keyCode = event.keyCode || event.which;
    const keyValue = String.fromCharCode(keyCode);
    const regex = /^[a-zA-Z\d\w\W]+$/;
    if (!regex.test(keyValue)) event.preventDefault();
  }

  useEffect(() => {
    if (
      location.state &&
      location.state.id &&
      location.state.username &&
      location.state.token
    ) {
      console.log("Change Password");
    } else {
      history.push(ROUTES.RECUPERAR_CONTRASENA);
    }
  }, [location]);

  return (
    <Grid
      container
      direction="column"
      justify="center"
      align="center"
      className={classes.content}
    >
      <Grid item container justify="center" className={classes.title}>
        <Typography variant="h5" className={classes.titleText}>
          Cambiar tu contraseña
        </Typography>
      </Grid>
      <Grid item container justify="center" className={classes.subtitle}>
        <Typography variant="subtitle1" className={classes.subtitleText}>
          Por favor ingrese su nueva contraseña
        </Typography>
      </Grid>
      <Grid item container justify="center">
        <Grid
          container
          direction="row"
          item
          xs={10}
          sm={6}
          justify="center"
          style={{ paddingBottom: "3vh" }}
        >
          <TextField
            fullWidth
            value={password}
            placeholder="Nueva Contraseña"
            onChange={(event) => {
              setPassword(event.target.value);
            }}
            variant="outlined"
            type={showPassword ? "text" : "password"}
            size="small"
            inputProps={{ maxLength: 20 }}
            onKeyPress={onKeyPress.bind(this)}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onMouseDown={handleMouseDownPassword}
                  >
                    {showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              ),
              startAdornment: (
                <InputAdornment position="start">
                  <LockOutlined />
                </InputAdornment>
              ),
            }}
          />
        </Grid>
      </Grid>
      <Grid item container justify="center">
        <Grid
          container
          item
          xs={10}
          sm={6}
          justify="center"
          style={{ paddingBottom: "3vh", paddingTop: "3vh" }}
        >
          <Button
            border={15}
            variant="contained"
            fullWidth
            size="large"
            color="primary"
            className={classes.button}
            onClick={Change.bind(this)}
          >
            Cambiar Contraseña
          </Button>
        </Grid>
      </Grid>
      <Grid container item xs={10} justify="flex-end">
        <Snackbar
          message="La contraseña debe contener al menos ocho caracteres, una letra, un número y un caracter especial"
          handleClose={handleClose}
          open={open}
          severity="error"
        />
      </Grid>
    </Grid>
  );
};

export default ChangePassword;
