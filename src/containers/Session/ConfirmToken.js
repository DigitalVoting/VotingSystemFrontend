import {
  Button,
  Grid,
  makeStyles,
  TextField,
  Typography,
} from "@material-ui/core";
import React, { useState } from "react";
import { useEffect } from "react";
import { useLocation } from "react-router-dom";
import { useHistory } from "react-router";
import * as ROUTES from "../../routes/routes";
import authService from "../../services/Authentication/auth.service";

import Snackbar from "../../components/Snackbar";

const useStyles = makeStyles(() => ({
  title: {},
  titleText: {
    color: "#2E3E5C",
  },
  subtitle: {
    paddingBottom: "5vh",
  },
  subtitleText: {
    color: "#9FA5C0",
  },
  button: {},
  content: {
    minHeight: "100vh",
  },
}));

const ConfirmToken = () => {
  const classes = useStyles();
  const location = useLocation();
  const history = useHistory();

  const [token, setToken] = useState("");
  const [open, setOpen] = React.useState(false);
  const [times, setTimes] = React.useState(0);

  const maxTimes = 3;

  function Confirm() {
    if (times < maxTimes) {
      authService
        .confirmToken(location.state.id, token)
        .then((response) => {
          console.log(response);
          if (response == 1) {
            history.push({
              pathname: ROUTES.CAMBIAR_CONTRASENA,
              state: {
                id: location.state.id,
                username: location.state.username,
                token: token,
              },
            });
          } else {
            setTimes(times + 1);
            setOpen(true);
          }
        })
        .catch((e) => {
          console.log(e);
          setOpen(true);
        });
    } else {
      history.push(ROUTES.RECUPERAR_CONTRASENA);
    }
  }

  useEffect(() => {
    if (location.state && location.state.id && location.state.username) {
      console.log("Confirm Token");
    } else {
      history.push(ROUTES.RECUPERAR_CONTRASENA);
    }
  }, [location]);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  return (
    <Grid
      container
      direction="column"
      justify="center"
      align="center"
      className={classes.content}
    >
      <Grid item container justify="center" className={classes.title}>
        <Typography variant="h5" className={classes.titleText}>
          Verifica tu correo
        </Typography>
      </Grid>
      <Grid item container justify="center" className={classes.subtitle}>
        <Typography variant="subtitle1" className={classes.subtitleText}>
          Hemos enviado un código a tu correo
        </Typography>
      </Grid>
      <Grid item container justify="center">
        <Grid
          container
          direction="row"
          item
          xs={10}
          sm={6}
          justify="center"
          style={{ paddingBottom: "3vh" }}
        >
          <TextField
            fullWidth
            value={token}
            placeholder="Token Recibido"
            onChange={(event) => {
              setToken(event.target.value);
            }}
            variant="outlined"
            type="text"
            size="small"
            autoComplete="current-user"
          />
        </Grid>
      </Grid>
      <Grid item container justify="center">
        <Grid
          container
          item
          xs={10}
          sm={6}
          justify="center"
          style={{ paddingBottom: "3vh", paddingTop: "3vh" }}
        >
          <Button
            border={15}
            variant="contained"
            fullWidth
            size="large"
            color="primary"
            className={classes.button}
            onClick={Confirm.bind(this)}
          >
            Siguiente
          </Button>
        </Grid>
      </Grid>
      <Grid container item xs={10} justify="flex-end">
        <Snackbar
          message={
            "Token incorrecto, le quedan " + maxTimes - times - 1 + " intentos"
          }
          handleClose={handleClose}
          open={open}
          severity="error"
        />
      </Grid>
    </Grid>
  );
};

export default ConfirmToken;
