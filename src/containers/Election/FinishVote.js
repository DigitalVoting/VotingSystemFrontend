import {
  Avatar,
  Button,
  Grid,
  makeStyles,
  Typography,
} from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import { useHistory } from "react-router";
import * as ROUTES from "../../routes/routes";

import Snackbar from "../../components/Snackbar";

const useStyles = makeStyles((theme) => ({
  title: {},
  titleText: {
    color: "#2E3E5C",
  },
  subtitle: {
    paddingBottom: "5vh",
  },
  subtitleText: {
    color: "#9FA5C0",
  },
  button: {},
  content: {
    minHeight: "80vh",
  },
  avatar: {
    width: theme.spacing(15),
    height: theme.spacing(15),
    margin: theme.spacing(5),
  },
}));

const FinishVote = () => {
  const classes = useStyles();
  const location = useLocation();
  const history = useHistory();
  const user = JSON.parse(localStorage.getItem("user"));
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");

  function Finalizar() {
    console.log("Finalizar Voto");
    setMessage("");
    history.push(ROUTES.ELECCION);
  }

  useEffect(() => {
    if (
      location.state &&
      location.state.candidatesList &&
      location.state.election
    ) {
      console.log("Finish Vote");
    } else {
      history.push(ROUTES.ELECCION);
    }
  }, [location]);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  return (
    <Grid
      container
      direction="column"
      justify="center"
      align="center"
      className={classes.content}
    >
      <Grid item container justify="center" className={classes.title}>
        <Typography variant="h5" className={classes.titleText}>
          Constancia de Votación
        </Typography>
      </Grid>
      <Grid item container justify="center" className={classes.subtitle}>
        <Typography variant="h6" className={classes.subtitleText}>
          {location.state.election.title}
        </Typography>
      </Grid>

      <Grid
        item
        container
        direction="column"
        style={{ margin: "10px", paddingBottom: "1vh" }}
      >
        <Grid item container direction="column">
          <Typography
            variant="subtitle1"
            align="justify"
            style={{ margin: "5px", fontWeight: "bold" }}
          >
            {"Lista Seleccionada:"}
          </Typography>
          <Typography
            variant="subtitle1"
            align="justify"
            style={{ margin: "5px" }}
          >
            {location.state.candidatesList.name}
          </Typography>
        </Grid>
        <Grid item container direction="column">
          <Typography
            variant="subtitle1"
            align="justify"
            style={{ margin: "5px", fontWeight: "bold" }}
          >
            {"Elector:"}
          </Typography>
          <Typography
            variant="subtitle1"
            align="justify"
            style={{ margin: "5px" }}
          >
            {user.names + " " + user.lastNames}
          </Typography>
        </Grid>
        <Grid item container direction="column">
          <Typography
            variant="subtitle1"
            align="justify"
            style={{ margin: "5px", fontWeight: "bold" }}
          >
            {"Fecha: "}
          </Typography>
          <Typography
            variant="subtitle1"
            align="justify"
            style={{ margin: "5px" }}
          >
            {new Date().toLocaleString("es-PE", {
              weekday: "long", // long, short, narrow
              day: "numeric", // numeric, 2-digit
              year: "numeric", // numeric, 2-digit
              month: "long", // numeric, 2-digit, long, short, narrow
              hour: "numeric", // numeric, 2-digit
              minute: "numeric", // numeric, 2-digit
              second: "numeric", // numeric, 2-digit
            })}
          </Typography>
        </Grid>
      </Grid>

      <Grid item container justify="center">
        <Avatar
          alt={user.names}
          src={
            "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOAAAADgCAMAAAAt85rTAAABQVBMVEXjHiT////XtW0qKinhAADcuW/Wu3DXuW8nKCjXt24KFSLWvXHgvHHkABzXtGvat27jFx4fIibVsWMjJSfVsmXiEhoAKyniAA/sHSQcICUWHCTiExsTGiPiChTiAAjUr130vr/99/e0mF58a0eMeE1AOzDaml8EEyH65eXysLHYrWntjI70wMEfKin88PDdwIf69/DmTVDsgoRFPzLfZEPm0qzGp2aojlnu4MbztbbreHrpZWfgxpPeckrgWD2AbkjbkVrlPkJwYULcgVL2zc7vmJnhRjXqcXP41tZhVTwXKiny6NbchVTZpGSchVTalVzkMjbmS07dd03AISXgUDnoXWDv48zRICXhPjHjzKDulZZ2JidoJyhaKCiWJCc2My0AAB1USzdOKSiuIyY7KSmOJSemIyZmJyhRKCiUJSeDJieQQ1HbAAAgAElEQVR4nMVdB1vb2NKWZbnIVcY4xsZ2sCEB03szvYQWiMmFLGELydZ77///Ad/MnKJeTOB+8zy7ScCW9GrmTD9zlNir0+Li1ebm2tednYWFBR0I/tjZ+bq2uXm1uPj6d1de8+KLX9Z29LdDQ0PDw8MFJJWI/go/gl+81XfWvrwqzNcCeLX5VQVowwKUHwFSgKl+3bx6pQd5DYBXawtvgWkh0GwwgZ1vF9ZeA+RLA1zc3EG+RcdmRTk0tLP50vL6ogAXgXXPA2eCBEa+KMaXA7i4qfuj01OSkhbywai/IB9fCuAXb94Vi4ki/HF9DPQZaBdplWgPyBMh4+OXF3qwFwG4uDY05EJXTCRyaudmfL2o6h/zhhflEeC6mkvQW7BjHBp+GVF9AYBXO2+HHY9H2CYunjL1+mSHACpelC/oamL/29PFRAdROi4y/HbnBdTqDwP8ojqYVwRwG/srmUatnI7H4w3ioOEN8FpXize1dLnWyKzsbwBIOycLQ+oPS+oPAtx0wAN06xPfM/USYiNCgKljH4CfgIMbdfpculTPfJ9Yd2AEiJv/jwCd3EvkOvtPmVo5bqFAgB+Bg52G/Gy5lnna79gx/igXfwDglVM41/efGibrrAA/ewM0PqfgWw3rx9OlxhPw0QHxB9biswEuLljhpej/8XLcRY111R/gLqjRYsbxjXQt8/3GsRYXnq1Rnwvw61sLPD25C+pCLaplJ//i8QwC3PUBiIYw52I6yOrkOl42ZUJ8+/V/CnBzyGoYsp+UvIJ4i+sN18NmVH+AygcEuOIGONkBm6G/303q8ibDQ8/TNs8BCNJpgZecvkMr95CCh0l0Jr0AJv0AKgjwwiXY9fEc4FPhtV1bID5PTp8BcM0hnXl6egO5AUrfiZAArvoARFcmcVRyCuhKDuXiDr6UX9VNhIW3a/8DgIuqlX0gneLZjb0s/CA34dAZmWIQQHJlavZvpOvr+J1j8n4M5SBrYaI6MBMHBbhpZR+XTvG0u4Rwv2572m+JIIDXwMEbB8DMBi7Aa3Hl/F3BqmwGXYkDArSuPpRO++Meo5TmjuQDg1GrHwUCfI+WvmaznrUjXIC6+SHD+Jg17zq08IoAr6wRUfZAcT52/oAQfmeLqtQoHXXIhU7u+QE8QOuS64yXpHOXfpcQC9D82MO0GVgVhgcy+4MAXHtrlc4PHhFC/j1KU/FdGXiXHpdOly9A45ikDyKrzlGtjlY0nbEsQMuFP1tW4kC6ZgCAFvHUU7s+ARAz+Om46VLqejL1wRsfuDJZvr4A48ZRo17O3NgWoIWJ1yYTBxHTyAAXVdO2p9zSKWmaEEqHWU8WPt55vwxC+PB5OskxJnLFjQtcgGrK891ZLMZwdG0aFeCVzfP04R+Rbvlccvr4Q973XTCI+Yfj6SR/9iIt2ay3RBvKJ6lsCkNRF2JEgJty+RVRhApBPOHORyobjk5g/PCxkJT8SfrE/8BESxInqr2IBtBUL6DxgvQ+Pu5dFtZo9vrzQyR0AuPdR5Vh1Kd9356xaslSRVQ1kQB+leolNzGJvpeHFrC859UB0fGv5e8OdMCYfPD/yDQuValqIgUYUQDuCHzFxEUjXpuAW2T99CK9aGVgdALj3kHWXziIgcUNVUTDQzsvA3BBqM+i+g6clHQ8h3o0gIU/QEbe8H83xEA1804iHI5gLsIBSnyJ9RIFNnX0FZODP3ulQn9Um89jLwuPExO1cmk9ER1hKECJL9fJMG+q/B1Z6JNH8qBKs6UBHZ6dAUJj6XTmEL9a1bQBAZITkQB3J53p5CIjDANo4ruZFB4xelO6GlVGR3sz3dmpNlxrCiA1l2Oxc/iz2qV/KlqrEhXgXVKEHunJm8gIQwDuSHwTZiRbQncjyFIwYBX2AW1KXIwAPsJflkaV1lZsHv6pnYxtI18j4MPIQ83F2VuenJAIQzRNMEBpH3L71jgWLYXqb66AmprRO+tr+ODabCzWPd1eMkgmq6cxYiH8FPFWY7ETTTH6La0ZBvBDFlWoCDYz+wJhiLUIBLgm8Y3bkpdkKZJ3/g+jbRPbTpCJIIyxfqtpGMYo/KbVxV/0Kto5wqz0YrHHJqKd3daCMWIWXM09mRnzcYkw0OIHAZT+mQMf6BlKevmyUDvhV5ivslXXAyXTXOqNEj9hPU5pILjAOuTn/aii0YdPlqua/4J8IAZansNEGOi1BQC8kvj2HfjAUoApyvq5HCiU5z3jfip2ChxELp2cEEdp0cXOYRmeafOxsRa9CeDjGaxPuuX5th9CCo3tGcaGlNK3AZ63P8BFKZ/OPBKoMcx7eRTFmKW7xYVVMSp90JEGLDB5SeAniOaJhixsx7oAEP+maGMgxNo2vJZYt+rHQXRiOvYXnZGaZsg/evIHqBakfXDig2t3gIW6Q/lVWtv4fLjoUC+Oatptd8lAAZzqzpwdkpIB0RzTZkBoY7HTKmLfatEHNKOiaWcnt/A9w0NQsUwDDHRkUKW1KKiDAxQG0COXCxxMY2o9aS85jIKkoQ1ncqdUlrrzxBKQxnOtKYwGcg7+B7+aaTa3Y7HlJsrw1DboUcCI+JbaZ5rLbmAkmDiqO56DMuDB5tAPoFCgifWMO7OeJmdJd2QijEPS+Rwgl0wwdiCV8H+jWW1ppE9Oq6h3YrHtJqrUQwNMItLJcrNFr+Ac1uKtAyJP2F04U6iUwQlUpT4ApYJRJ2tufDW8LMQ18iGMFqp4DZ73vkIiCl6ZNjU1c4IAUOdoxtL26dZ5r9JCziFTQUpHEYuGTJ0fo7viCkSNAzSr2FcjS9glVhxZ8HRJeN5+isYHoAwAi+tHGUf5h2We9eQHia+izD7iwqmSt1K5Z5wEb/MMH7ralZd9rPaRc0oTUdwaGtp8/HgXPjqG4qoQ9jPUqTN2XzV/TambJ8cyLL8TjvfQIAAXzARMMaeON2o29UzOfNLMXBrwiLH5nmZUH+kh8RnhXxA3TCHA5gxds30+2+1V+qeod8h/61du8dcop/Aj1DGooeDDY5q23UZRtq8Als5KO1ZM7UIoGu9l6AlwkzFQ59mVhDpRq4vrgivvysySxMXOQU/Ox9qMhbHTfn+5zRyxpe5M7xADiipY+mqVvogqtQI6ZrsCEtpmXgwqT7T5sP6aeElnuFHAaGK9bkNYbsTlMvS0914AF9kC1N8rH5Mpwpgo3sTZhQW+PWEDmxVmy5G2UCZPQe8/ikt18SGBlRW3O22gx9rr09sgL0Ys49iYohlkGx1fetAdFbp0bfKiY5a733pZQy+AOgkolgfyymfOxkTi5qkBodikA5+23KswzUe6cBn+AmC0M+aL3obFfOiekoWHD8/0ATR/LwrCdn7ZUaFL10v7tmp+QY8GcI1ZQJadNIzdQpIvxo2VDMe3KvBVAZZhkIXYMk4EH4GnWv/+VnObMy9qwuojjN0qKtzlpTa7zqnLq7FW6MqZlRveO6SLlOOwh61wAxQCKrIuRn51mnGxmOts2PEprTEW5qFEHWr3zKOkiN0YHSAzARqmNxa7bd7i1VqwNNtcusF8WjMc+VVCCFovcyRkU89e7xlJXyF1A2QaVFctLy6/x0vJRcJnzWujOoRHQdk610Y11CtkIwYmwGjgxXoVFId7RavSD2+Xt/sWQRAVugmVw0umPj7kjTwr4nhpUhdArkHt6XMjf/delEls+ECE4TvggqKyh9XY1E5j3dFn4GMgZ1k6A+w8vSOtR+r5RDGdU27whWxO7xrsaZJ+mtQFkFuIT85IIf/wiSpY2c/23zRRHRySDzZPIbvWCgTxr6BfakvobWssbaNop+KhDi0I3/MXraey7+9EAjb/MeVj7p0AvzINo3vcPf9wkEy5KndkEwAamvOZ0LyDMjL390jArykrCjaQ8jVobGbvD/EPS7aUuzRJ/VixPgpn4bAzgeEAyDWMT99HXvkoYkALFHSuYdnBY02FLr6R3y7nwj6D67qN/gE8z7I2arQO0bmx/H4aSx+r9uw5xVNeesYBkGsY39KDeJPao+WO6HWcapWz0wjK5TI+998gFiJh5o2CEO6PovW3XVo/8ChbJb09NjvAK65hgioPdMvT2JTJQ4qM7iuVCAL681w8/uansI9VlZOqUokJiUDX/NZWs/eqngsWOiqHdoAsik+5NIyD0FJtWXRJ9SwWQTyBfnoDJnru1zAWQvhFEvrITD368LfhVlWwUPUHKEyEfwGLqMnioabpaWjd2Sj4Rn6fQx/rTaAm5YTWkCstL8fbTZSWcpsKG0DGQD2kcjTaJ35pM13zrtVItv2PN8yJ/DOUhQolN5jHgP7bWJTrZ71YaAXIGRhSODIqEN9AsIdpo/BVZyNM5oAXGb8MNBWcMGe6rFVbiI+SWIZHSGIlbxZaAUZjICrx/mjlkNcaonucI39dpldW0uWjUvwyyufRcTs/7bZRg2Go3z87DObjgxcLLQC/RFqB6F7Pw53asXkIXbXTpchcBA3T6Kyk6/BfuKkghOf8ye4xoYrh2Ek16H2aLPziCZAz0D8jz6iKt+1NUTyK6nQpIg9Hfpkrr+TeAcBOJoKpIIQzGJ6MoVfRZDF0sK72YqEJkNvAoJoKvy1LgQHrmkvca4xC/3oTn+wQwNxFOYKpQGpCmK+1IGRiC2OZBZu+lD8QHumVB0CeaArqnxAI0QkGu8DVaTQa+TNdvsghwI3E+mQ0U8Gp9bjcAoBTmrbszmPYSLJwwQ1QxLnXSgSE27getOY8qtNoDzny9yVWhhnA3HgpmqkgwlpHD3Vp39C84nwL5T+lnB6pBMgTFRBjHQc1gbC6bQtjpPNzWxwTQnPx2n5OZQBVtZ6+/C0qwhZg28Z32m0pdg/KTQ8iqFhzATRbCZOFPT8mVrXe2S3G2s1+O8bVdyQa+e9cugbX5gATE7VopgKJpBODxDYGUIEhWf5O9IMNOQFyG0GJcD15/eAJkaUD2xA5ACen0A5HfUgwEXVslBQcTMTTcz9HZCGWFDVKGdxr5/NBtzQOZLeetBQCoMhlswYNkFOPxdUSEXb7Hpxh7bwbGd/Ir3Ppp4TgYJHVaqOZClYiPmxi/hSc7wAdk/8wbWnuXrAD5CqmePPtiDWDpdRVJxPR/Z3aPqP8JxYCW0HL3U5gIhqISwLEWt/cv6OyEJTZIQaFIJ4B+I6JfUVRjFm0AdxkKgZuW6rf5LzllMpgzaamYP7zcJB2JTQRtBfCBFjsgKn4I9rX8c2yhw2Ax9u6czfsRurwpg0gS2YX1zOYMH7icpr9aJVTDDvvUWkaWKGNFB4JfL9dxnEjqBUgbnhJxyOykBJbsdiZv37J71JGrKheNCZZrUKkuRWrhPI9GunJCy6nukVOQVAEKjBK7UFyn5fxMq8BmQDxZUY2FRXjdLbb98VnKNcknrlOuQTWKGGVUcVqBEUfUbzUmMgliIlSTqnKd8p6WSDEHgAg5ikavAQEAGsb/K/7EaMKglDV/P3s/CrtUysmjrBqQd2QpilkAJmfbelisMjpAbvKKCVhWfkcFPcA2WvMU2T40rcAVNVSdFMRRAbb/JNbf2LF6AZbDdzjVmwSaikQpzNMTvUUZ2FFo0Lm+aFWWaIsdkSiPEVGlQDrAmDiph7ZVDhotGWK6wfa+VPMTYhmwZJNRhWrDn2y1xYz2LFliX+rVTIRW+BaRI4hlBHKU1gB3ggOwv3mfh+chUZLux+TDg3r406sr8gOjPSTVY8SwB1Th9oQrji7CrVDHoMOYCXoUp4AixgYRjQVEl1Vu8X2FDOKQfezeJOx1O5560VhRwJkbho6iPby94SrtdfQznApRsoBEWGeouwDUM19j24qOLrD7jx7xdKloT2I4zXnc6vcH0WAPNSF29kBUuOrM8CvYDQYkv6x0E9v0vGjsg/AIgSGl/+JjFDrn7ICZBtQyrAJt+gVN6wAqSVZ5WGvYhoJ1SGhrPvcXaVoKQEW10Ejv8zVb8ZLPgDV3FEp/iYyPlFBvte0rimjmIkp2veq87uRoVCko53YcDRKlXALiFcKyoisQcEJhSV/ZANYswBU1UZ87peILCRLPNvTWqMUAws1YOyCFk3YmEMBC3e4ESA3EuOOLqI61uMD+3pDCZzQxkYQQFz3UbMX6JL2eaOeZgkM71xbuYk1aCgYQLEEndu9G8WBmuu98P19Cesh4QWwyFshE+V4OqqpAPMkVp4td5F17nROr5iLUJFWUHV0FZL7KHNszVarNfiGh7k4uL6eABNHzN9Aa3/5RzSE1VNWRAZFt0XtcIxwv4hjI/AkX4SbBPAr99McS5AeRGcSWtG2u2PdZSVaX4igkf/OYYO+N8D9Fel+x9PRrodlumWtpVEXjhnaYze34+nr3Fv7SgCZI5qYcCxBfAt8B0/zvs0UmLUdIJzARDRU1QfgxDfW65kAa3/5VzQWoh6d7Z44XA0a+WGXvxKzhOiOKkLHOAcSpE03ZnQpJuk2egvFyK9zZHJdABNMNAULwfxGdEkNhb/o2HnffNNsIobNzRTR2VsEKHSMnX9WNwbrc499ZekU/mwH91BY6Y836XTRi4MIsHhTb4jAcDKyqTAUzKrPj93b1go6awkHf6SWUaSOsVqScrn0Dd0YSnOP3rPuyNEqJu0fI2cK42AiEtz+uAFu1NPvOAvh95FNRUsDf62FYmTIAmzevcKEqd8EgMyPwVWaTpdLtXq9kYl/P9qfKAo3pmq6DdR+HRHfb5dcW3tzEO7XEIFTIz1AopuwgaY53BaGAjf9ONwUrmXAl1FEKHHzLRNfudi/2eisJ3K5BNstTG4MLG2Rq2h1oxcjZB7Gm4P4Qnlck5ioR6uJSmzLXYpq+HIxsP9g/Vu9VquVSuVyOp2Oc28CAgpFbB9YXy8SrqLFz2BuDEu8CoAROTjy85xY6d4A0XukXLBKaeB4ePsMYVma2ZKbvXDTDP2QdoZ2Nm4mJsaPLr6vPJUz38aFGlViov3cObtN15NsWliVcsp0KRBR/60bNvrpjQjLXAAnJMB0nAHENPBltJqoQNc+h4hC6AMqKhWLxQRSDkmAeBtTFoecuFLJZDarX7//+Jk52tQGM6OhhR2L1M+hYJ4iXRM7bzwBMqUmWIhuYiRTgVsQ2icQNT1q2qwMS/M+IwRxS4wEiLiS2dT0+4PPq3sPSh5IQGEp+5OtsXbUVskRMBElcZMAgPE0Z2EHTMU/EVjYXAYXRgNP7USrPkp9kJ9OpvSUrqu66iAAyM1g6uB4V+By8chs+5uKaCTSkjl+AHMEsM7RopsRxVRgnNRkMNF6cYDG3ufjjwfvr6endTH3kvWagyFUuBlM4gY/3+u2+pQpaHejOaOYp3gn14EPQNqqkC6xdYr5oEimQsMNJdgF1TSqZn8ADoLkBCvyw97q6u40IRzeVHg4nwyO+8DAVg9ZF24EQg0jdhX5AmS+lRDY3HgtkqlgIRPBBI3nVywEvNcM4FoEgKBcWtVmZTRyE/bIP3MiLULPbs/JcEcYowhkYV18rBbJVLTG0CgTTDBf/iodMzUMIAuWAgC2Zs/HuqfLvVsF93ZEAYn9FOuWzFIm7gWQp7hEKQEDwwjtM80ZVC3V0y0Q1EPF3zHmAAtflZ0wgNq8NKztqanAJgdGI3+mS+MWBq6UPQFy75giKiGz4aYCIxtgWxXd0cCmoGkGcEdZCAVoeg4xVGGh+P6+NOVO7Lt1r8HEEXf/a/xl4AejdFqe9kIfwQJwIRwgSmZ/6Wz5tDt7PhWlqHQpNQc9N1WsvAAK91/+7nskU1GNFHMPAlBBrVTB/Y1ahPEM2E/xZOLj6XIPgLLSI+QZA8MIpsKg52Dkr9VNgNz4h5gJnEgBECOomJ/emGEQUibuA9AsFAinFfOLYTXR0Z5UCO35doAWZXZQ1RU1AsBWU+mfLc+cboWrmJFf7SaCq5JAgDQ6Agn1bUhNtNKzaoQxXzWKQwWJIgA0WluPLVYlDw8G/7CbCLEXzgqQ2YWiJdEnvoEl9JCaKALcGpudPTk5OT+f8u/rGgRgC8N4vhl+OUSLjoDzdWRh4Lu0L0BLvUREjmqxHNaUXznD7ZQR1qAaGSC2GLW1Zix2dtsOYyHmKRoSHhlvF0BegLXlGCQL0doHmgoCGPgMHKAILISSSfnbwTZvFVvCGnbIZBS7iVDl9mY3QNtYA8lCZHlgTbS5HQvs5pIAxQ4nYSb8ATapQUXD/7fQxw246sjPcyxTyJ/WrPl7ALRVvCQLOxBVBNVEKVKKApDFwKYd9AeoUBTfXN4+xHZN32kaSGQiJAPRrkUFKDUvWvsgUwEAI6W9BgCosRajZsXAXZD3AQBHfk+Luo54Vn+Aqr0jINMRgeFkoKkAZ3tADu6EAsSeCtq0iimZgIhw5I9L+Zxc2oIA2gvKJgvB2geZikq06kieNVaCs/01dA1iZxo6pLcnIS1qaVNZqI5qgQdAe1HWEiE3IiagAsngAL+KgDdgDVI4MU9BU1CL9shfl9LpUq0mwgmQl19zk7WShWoix4EuTuROyzCAZkSvP7CMU96dmjGaYoPGVD9Ah4KGKe2bDFRtEws8ACb29/fHOR0hiZeTSIeYikiUFABF7YVyhtfvD453V/c+KAZP4DDSerNT81NjvaB4fuSfOTNTyJZSMEA14SDB/AQEhgO1zwQB3BRpQ8ZGIH7qjD59/elA7hGpoF+EI95avhj/9caSKbSZCD+AvoRpYL/2mdEAHcOOWWGs4e33Q1c2gDbS9aRrm5axNe8TUIz8mRa1FHpGR1PRQACxxcunfabSO9O8GivzhvJwt7e6y9KjelYcCAQARWY7QZUXW+3FNgjWqLbIbfPO1NG+DzNTaJsPNjBACrK8TQWNCzpd0jRHQ0Q2m8xiupcluE0aWhTFl8TR96PxCSyeqVi+YOUzPssUZ9oZh6c4XmrML3d/GbeGgYl4+kcAkoB7Zi80ru5OZvqa2VJp+J1vhMUX0YMwXiuXWPkT64RH++gzs4MwjKXZcw3jsCp1+HsBHPnvnDUMdLX1DQiQ0sCe7TPG0qlQ6fNb20KnG8eCa8UiKzLl2D2ofCYKoOYzpdPlcomOE6ISNg78UdAPPaugs+a1nQ5NhCVTqGbSNrIDPAoHSGlgz/YZo6ppcoSSbFejwZy5XFFd72CZcP/oO2/m2rGUsB2rxtqEgIGudkIj0Ka8dkfhvg9LpjBxFH+S9O7pHchrIEA8XksU9gTR+/aQFYXWYVfb3mpb3CrsdZp4qmUyjUadyrwZSwnbqwkhLtpIaGQHYBvDahWOEPMs8f7xxh4G2p/VmbrnABPfMm5iD1gGjQxs92mfIe+/CfpOpBdYH8lK2e0aUhOCTxsJNQKxfmbwRtsaFq4UzTj1WIQjuPfYX+5QJ3oAzLkGCooFwsXax1SM0orBApMIvtnEfPvlLG0kPo1ANKCRGQos8dJAKjaC8cxha3Hfhy1TGAywLAA6x9y5ybN9BhVds2LrGL129nLZGoFkK5dD89GDMEOBpXlTe7li3jf8EJMBAXqc1uMkL1OBk+XOl7Ytgx5xHpD93BF7K9eOdzNeuiwbfvlwPogmZpcVZ4UJ931YTER0gM4JaR7kaSo03s8lu9WoYdTmOolmvJ2gdkp2LBu1bOOQRTCup7foQWgzdm8NnFBrpjA6wKdwgPaaqMH23YzeEkI5+4Sa8ezHU9nbKYWWeeecOUeGgoGZ6t5zHxAMhW3f4MjvphF4eYDWmqjRby8TxFFtu9tdMtspr3WHlRP1c94Q69fSTL4XP+yDhtoRNe25Q8xT1CZyztDHSgQwQT5GMYeTENinXS/Um4WmqcAu2PltgthsWRxuI+nUII6WZr+mdHrt7qMDYQlYN7PjcIpxGx056QlVMtLNzcZTOr0ywegpAj5LolvM89x2zlf1WIKOpvQ1v0WIut/VlY5qWrIQ8xSAsORDZSK6Zw38XCA81qVGTWXO46R8SLbPgKM/RU7a1JkNIluCNj9MLEGxrcCnK730Dn/q3lcARkOy8Kc3ki59ac6P0haj7s/Cf0kGbmt9msIw1dPM3Ak72shmBZ0bQ8QitLdc1lZUNkbUCbC6bZtH8JOgfwn6g+hvot+A/vOfv/76679IPyP98ssv//zzK9C///3777//CYSMmru8fONB8FOWvdBmafbXLc8OmRn2vHsJTphL0L45y8rn2ne2CfSDYw0amrIVeYfryMDk9dokA5voaJ8rOGDG7Mv94FqCDdfmrE3HBlCgOjk7uvpgx8fgwVKP3tn8MsRGx6MfutTUtizZbbYEJy1iLreAmtvrFl2GokGmWy84ikkto8s12f8YHzKQ5gZjlr1vrVPSjH/1wjJqWRoJc4Oka4srG5WvT9vv0moyeMvhk/dfHODo4RYbjX+LFQRLeSJPYyVzG3UppB5bXJ2blNlhB/q14y4Ej3kTYfOVXphG+y1DjFm3MxBElHLYCfV73SGh1k3KPLXGWzvYAPLUtR0DzgmPtWcQXktb6h0GDN5/aTIOY6zPUXtcMpg6NUmOCJhgYlry2mYuZJQGBUxuIL6ka/QRLACCV2FHKsz3/mcLEeOHcxwmrDQN3IO2bV8i+Y+0DTuxjmfuyA2ufOwRByhkFGw9S2+6k77wHqsYgTVvebjinIT9ekS9I+1b0twVjxp2/o5aDoqJ/UlZoRQTZZzDOhp1hs/reC4S2Qrt8+luP84Pstf8B6mpYC6BTXdpNt15PUP5xMS0k87w7YmOYR1y3EqGpoQ7p6RaqBpDcdGaVTYk839EVHyNjbFY20u/5VeTfJgFw+EctyIH5tDugqz/6XQ4r+6E7jN6GFKxfyGqVpvNSkU7JA9NCZpnkWTqlPmhzoE5lpFH1inMLrLwTaMpS69NWrf7uHzWgxDmhE4A8Bea/GfrUaExJ0ARM4F77TvSSbFFg1rYBKmXIE2kg0C10Mi6gJlOYDDESCCPoVWLYgdM6i7IhgMHOUBsC4q8Fe25VNkWzxc7HDWw+TiwH9BjEK5rcFwg/yjBxkRUQ20a6VXRDzEAAAsTSURBVESoHyIcuX62fXuCOSYsSvQCX6nxILpHYm6APOwNPFqQeb0nWqtFo0lwCTarr8dFGpNujFaa2EjWbJ1ZUmnexNtgvUf/iW1o/idzE7Vw6nSX9olAUFg1lk+Xm9GnVw1E2uHjTJ+uPYoNEL3QQYr5z5yBnsMbxfzUsAGqsnoFb7PJ9vy8iktjtGbltfkgztAo1GNeunuAapiQKloPbzeL00xF/230CWuRyVDm5XvElCj8rR3ScMxCQ/8BqiYLQw6mq9BWO5Yv3Xo0+ifugsyPEw3vPsSVjh6aoT2OnYYMKpBHoPqOwI2Jk148D/r1Jjy1jQ0ffFlCj6mnGRqKyD3P9YZ9R7QfFGJ+AAULQ6fEEvVFYI/J7pe2+LDq2AGMeERTpBmY5vRU/zHU8jiw0NMTFdwm1eauaDW8E3hgQrXCzpkCVrajXP2D5xRqB0DhcutepxU4H0F6pdit9xoiao7ZjrAChAm0TTB2AZRRk/csakOxuHFyNhfa/GcdEhJI/CgShTf/hF1fmMCQYf6mR+px4LahfNYtP8Z51FjjIT0w0CzHSES1FgpxjdZ823Z9z4OMRStQyHEM8lRTveC4Bp5QlLIemEXTOBWNfLYoBzEMSjSxsVelO/WtWiZ/l73ec0CU54i4zjz1ORLFeVZyXjlOpVgwJXP5VVRw1JbwCnZeYWciMAVqa2HN72V1PTltO1DD7OUKPRJFmgo1a1YlJDxEmJLZfI37MS/rqRlNUVfxVKDsZCIQJfWzOU/aUD1PKvAEKE2FrAwCvCTB472IuiyoNbXlbne5Ki0wDgr50cWo3S7LYSqYiHGk0PK7Wfmmkx/56Ex5pI3HSZK+B0vBO6Lza0x4ufUjdtpfwbxfs2UZo2gY3fPZJbvD0QzODzedG5BoeNqUyJuDArXnQFlaosh77fTsJzobRcwPj3awlJm8yD4YeTzfTcCbLJWfqGboqFkIfGw0tfWJDG35RCSmbHrQ4L+eOYfYy8IjcZqk/NG9DX7+I01q7NTEwVJ4bFZemsBoR4OJebgopDZ4WN0IQigm18mHq2r9Lf6whmbcVqTkabfsNKzmlAOO7Ac1BzbZ8LFTpTqZdC1zxM91A30jfLSoh7uZQqqKM884PES4Qsvw2mOpiQYdPgQFnBEKeLBQYtDBbycGj0BQCvGsA/jL49K5dQCIAOhpV/OU3c1tUDGwZJ7MJ/Rf5OP5zCOiObyLSbOAWqLKr1fMKPaji8QzAgYWIVx0S9pcYaBjwv6Ohw1rzb41Uya2fHvp5fx7widPzS1nVjZylh5A73NOw47IdMAjhFR7cgccrCZjUXvdMx5IYWPGGcYFj012bCT+fQl7bLtVo2Ktho2ynabbHnkeg07Msp6qHk834jcS3yBHZMbaMs/thBcXp24m3WcXscSCKW9V5A72z4JX1yaGnWismaCl4Qw6o6+xLmlLpF5dOpk/73kFX7Tr2H6qOkKUO76H2gMAlMfUFtcb7iaPOtW3PepPWu90uWrZHIOB4hIbejyliaH84KMr3VlyDqjk17Z70gYYDi/LQjld16nj7MRcWoCDHVMrl2HR6yDlOvVnZ91ni1TsOUQARhFVtQt/Vlts4U0x926KcQmH8SyFD6N7oD3HuSPnqeOTYrfboAcNW46K3vBA+K1TtO+q8CaN95ugX36+PBXjHIzN92Z42h+73Mc0I2wsZJ6yEQnnQcrxSbEdc/Cjoq2HfbsPM8c9LuGTOTHnxvRHlTLwJIxoLXstNBWkUvGUZU2ZCZkURefxJL67Niv8yGHfpjXMTTgFgxotww4QY2qVJzU0Y2amRalwdq4wqpZRxsDZ06mgY9oZfUg6xoAjNeRx7Z4WMAygeR62a2lP+lhCB8k5etXu2LKGh5qcVagjebmFaRy0H2JYlP9IA87CafeGr4bcqeGnYEIAxjZ9ELLRquF5qco532SBBzko/XmmcfDk5FuFGUg69qs9th16dJqx627YMvF5HqEcAaDp0dgR0k7rVIS4CNwUhpQHjrTDuXlm/qM1O0V90uGXwpRE0drRauLzVaDhAMWJoHaE6XLCLyvlRy2c6XrOI6lWz/xHK+AQAhtR1jNh7v0w8blO/RwEoEzR8OOLiajTxt1lGUi0w3LU6x9R6c6mZjJSv7iSMIMBlObQ4uRSp82Pjad+BtnUzOSNxOdrACMCtCBkYQrbBOIxQP2VyaJmeC9WJHzhAE2EEEjj5WmUe0gN8VVIqJlyTW41DccXAaCJsLj+VGJjwtwHLb8+MTVTSpee5DabCPiiADQ1TbF4UaddLkl33tvyJM+XXiPou0zNTF7IbcZh+iUywNiOjPBzE6F+9sP18V3+OSCNvLL7yd3kKImpGXMWwVAUfNEAxtZklibXCZmAn7/WU8nU+92HgUAa+fzd8XQ2qQe0kZGaKcocxdtA+z4gQNNro8Ro0t/PZgfGY1a2cLAaUVqBdauf9CTPHQVc3LrXOtA/GxwgeN4F8+LT/i1xd7JfTNeT2enjvcBlRaz7cHydFejwe74mVtZwIT4K8q+fBTC2qA7LZ0hNf/B7CJX2Q8u5FHoy+f6z74cVYN2BmhWPXUzQ+vLsVaVP3wkWDqv+8dFzAYK5sDYkfvI8SZMaORIb3yfWLRhhSfocnJo/yIpZtcViLtEZf6KJUFnPbjlMsgv1EsE8PAOgRdUgaz67ZS+/S49QK9ca5aMbAMk1gl9oJXLuwLr1m4tGvZSuoRHSVS94x1KMI6qXwQHGroYsCzFZcLWVUiscd6fK9cy78Q2VMrO+ACkTD6zbf8rU+IxADMVcXR6GBV5hKOryGxxgbNEqpq4TCmlgYm7CPMKqVM98x5n5vsExbvDfuKjXzf3K6bpKX7Be2DCouCzEM/LyewZAFFMLE/HkN/PRqUqXcOQVyt8S/gAfaIN/zX4gGSXOLbYC4Un7UBhIPJ8D0KZNQZ+mdsVSNPaodOeaQxIAkCYYxB1Ux1BP2gojv8tLQANqz2cDdDBRTU7f8WfB3GXuwrmxM+MvotRclnAlJWnPOqu+AryCCW9w9j0PYGxRt00Ryr7HpZjH2khiw/W4QQBxZ5zzRDnMuiaYrTDyqxZ46pA+MPueCRA7FaxyiofaUiOOVyUjACB2WDsO9SJitqKQX522wBt29xe8IsDY4o5NTlMqWUCaOD0AQPdJH/w7G+wcWfMGhbc7z2Hf8wGCTVywGkVS44lxr4cNAHhtH9QjRLQ0uWKfbVIYWhjI9r0IwFjsi+oc6DUeb7gmcOBmfD+ABd01IyRdasT31+2TI4bUL+FP8woAYSkW7BATuQ5gtNu1IIDOQ8vStUZ8vGO6sQxe4XmL7yUAEkTrWiSvcmJl0uKaEEDvHBzZeTHGBtyeyRWrk86F88fg/TBAgKjbIVLQszH+brJeK6eDAdIGcTz6rFyrTzLH1T61pTCk/yC8FwCI6ubtsKq6QHYmLtKZRu2bL0Cy87nJeiZ9MdFxgQPD8Pb5quVFAYLR+DrkYCMuyEQut965OSKAeUOSuQTx6Ed1orOek3GVlXlDX59pGOz0IgCBNoGNLow0UUzFkSZ7q3sm3TH6cEBbGj1mCRWAeT8sm5xeCiCwcU0d8sKIJM+asZHrjB2ObkhdexHmEb0cwBhh9ORjdALevSS62AsDBFoEWfVlZBi4IZDMF0UXe3mASFdrA4MkcGsvoDRd9BoAka42d1RAGQ6zMAzY1J3N1wCH9FoAia421xDmEAAdLgBJUEDwI/gFQFt7NWxErwqQ0eLVl821ta87CwtcbeoLCztf19Y2v1y99ILzoP8D7TOgVv6DUosAAAAASUVORK5CYII="
          }
          className={classes.avatar}
        />
      </Grid>

      <Grid item container justify="center">
        <Grid
          container
          item
          xs={10}
          sm={6}
          justify="center"
          style={{ paddingBottom: "3vh", paddingTop: "3vh" }}
        >
          <Button
            border={15}
            variant="contained"
            fullWidth
            size="large"
            color="primary"
            className={classes.button}
            onClick={Finalizar.bind(this)}
          >
            Finalizar
          </Button>
        </Grid>
      </Grid>
      <Grid container item xs={10} justify="flex-end">
        <Snackbar
          message={message}
          handleClose={handleClose}
          open={open}
          severity="error"
        />
      </Grid>
    </Grid>
  );
};

export default FinishVote;
