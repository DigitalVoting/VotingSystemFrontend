import {
  Button,
  Grid,
  makeStyles,
  TextField,
  Typography,
} from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import { useHistory } from "react-router";
import * as ROUTES from "../../routes/routes";
import CryptoJS from "crypto-js";
import JSEncrypt from "jsencrypt";
import electionsService from "../../services/Elections/elections.service";
import cryptoService from "../../services/Crypto/crypto.service";

import Snackbar from "../../components/Snackbar";

const useStyles = makeStyles((theme) => ({
  title: {},
  titleText: {
    color: "#2E3E5C",
    margin: theme.spacing(1),
  },
  subtitle: {
    paddingBottom: "1vh",
  },
  subtitleText: {
    color: "#9FA5C0",
  },
  button: {
    padding: 0,
  },
  content: {
    minHeight: "80vh",
    paddingBottom: "0px",
  },
}));

const ConfirmVote = () => {
  const classes = useStyles();
  const location = useLocation();
  const history = useHistory();

  const [token, setToken] = useState("");
  const [message, setMessage] = React.useState("");
  const [open, setOpen] = React.useState(false);
  const [time, setTime] = useState(0);
  const [newToken, setNewToken] = React.useState(false);

  const user = JSON.parse(localStorage.getItem("user"));
  const waitTime = 45;

  const calculateTimeLeft = () => {
    let difference = +time - +new Date();
    let timeLeft = {};

    if (difference > 0) {
      timeLeft = {
        minutos: Math.floor((difference / 1000 / 60) % 60),
        segundos: Math.floor((difference / 1000) % 60),
      };
    }

    return timeLeft;
  };

  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

  function Confirm() {
    electionsService
      .confirmToken(user.id, location.state.election.id, token)
      .then((response) => {
        if (response.data) {
          doTransaction();
        } else {
          setMessage("Token Inválido");
          setOpen(true);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }

  function GenerateToken() {
    electionsService
      .generateToken(user.id, location.state.election.id)
      .then((response) => {
        if (response.data) {
          console.log(response.data);
        } else {
          console.log("No data");
        }
      })
      .catch((e) => {
        console.log(e);
      });
    setNewToken(false);
    WaitToNewToken();
  }

  function WaitToNewToken() {
    var dt = new Date();
    dt.setSeconds(dt.getSeconds() + waitTime);
    setTime(dt);
    setTimeout(function () {
      setNewToken(true);
    }, waitTime * 1000);
  }

  useEffect(() => {
    if (
      location.state &&
      location.state.candidatesList &&
      location.state.election
    ) {
      GenerateToken();
      WaitToNewToken();
      setMessage("");
    } else {
      history.push(ROUTES.ELECCION);
    }
  }, [location]);

  useEffect(() => {
    setTimeout(() => {
      setTimeLeft(calculateTimeLeft());
    }, 1000);
  });

  const timerComponents = [];

  Object.keys(timeLeft).forEach((interval) => {
    if (!timeLeft[interval]) {
      return;
    }

    timerComponents.push(
      <Typography variant="subtitle1" className={classes.subtitleText}>
        {"Generar de nuevo en "}
        {timeLeft[interval]} {interval}{" "}
      </Typography>
    );
  });

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const doTransaction = () => {
    cryptoService
      .getUserPublicKey(user.id)
      .then((resUser) => {
        let transaction = {
          userId: user.id,
          candidateListId: location.state.candidatesList.id,
          electionId: location.state.election.id,
        };

        //generate AES key
        var secretPhrase = CryptoJS.lib.WordArray.random(16);
        var salt = CryptoJS.lib.WordArray.random(128 / 8);
        //aes key 128 bits (16 bytes) long
        var aesKey = CryptoJS.PBKDF2(secretPhrase.toString(), salt, {
          keySize: 128 / 32,
        });
        //initialization vector - 1st 16 chars of userId
        var iv;
        if (user.id.toString().length > 16) {
          iv = CryptoJS.enc.Utf8.parse(user.id.toString().slice(0, 16));
        } else {
          iv = CryptoJS.enc.Utf8.parse(
            (user.id.toString() + "ABCDEFGHIJKLMNOP").slice(0, 16)
          );
        }

        var aesOptions = {
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7,
          iv: iv,
        };
        var aesEncTrans = CryptoJS.AES.encrypt(
          JSON.stringify(transaction),
          aesKey,
          aesOptions
        );

        //encrypt AES key with RSA public key
        var rsaEncrypt = new JSEncrypt();
        rsaEncrypt.setPublicKey(resUser.data.rsaPublicKey);
        var rsaEncryptedAesKey = rsaEncrypt.encrypt(aesEncTrans.key.toString());

        cryptoService
          .doTransaction(
            user.id.toString(),
            aesEncTrans.toString(),
            rsaEncryptedAesKey
          )
          .then((response) => {
            if (response.data) {
              console.log("Voto Realizado");
              history.push({
                pathname: ROUTES.CONSTANCIA_VOTO,
                state: {
                  candidatesList: location.state.candidatesList,
                  election: location.state.election,
                },
              });
            } else {
              console.log("Voto Fallido");
              setMessage("Voto Fallido");
              setOpen(true);
            }
          })
          .catch((e) => {
            console.log(e);
          });
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <Grid>
      <Grid
        container
        justify="center"
        align="center"
        className={classes.content}
      >
        <Grid item container justify="center" className={classes.title}>
          <Typography variant="h5" className={classes.titleText}>
            Verifica tu correo
          </Typography>
        </Grid>
        <Grid item container justify="center" className={classes.subtitle}>
          <Typography variant="subtitle1" className={classes.subtitleText}>
            Hemos enviado un código de verificación a tu correo para que puedas
            confirmar tu voto
          </Typography>
        </Grid>

        <Grid
          item
          container
          direction="column"
          style={{ margin: "10px", paddingBottom: "1vh" }}
        >
          <Grid item container direction="column">
            <Typography
              variant="subtitle1"
              align="justify"
              style={{ margin: "5px", fontWeight: "bold" }}
            >
              {"Lista Seleccionada:"}
            </Typography>
            <Typography
              variant="subtitle1"
              align="justify"
              style={{ margin: "5px" }}
            >
              {location.state ? location.state.candidatesList.name : ""}
            </Typography>
          </Grid>
          <Grid item container direction="column">
            <Typography
              variant="subtitle1"
              align="justify"
              style={{ margin: "5px", fontWeight: "bold" }}
            >
              {"Elector:"}
            </Typography>
            <Typography
              variant="subtitle1"
              align="justify"
              style={{ margin: "5px" }}
            >
              {user.names + " " + user.lastNames}
            </Typography>
          </Grid>
          <Grid item container direction="column">
            <Typography
              variant="subtitle1"
              align="justify"
              style={{ margin: "5px", fontWeight: "bold" }}
            >
              {"Fecha: "}
            </Typography>
            <Typography
              variant="subtitle1"
              align="justify"
              style={{ margin: "5px" }}
            >
              {new Date().toLocaleString("es-PE", {
                weekday: "long", // long, short, narrow
                day: "numeric", // numeric, 2-digit
                year: "numeric", // numeric, 2-digit
                month: "long", // numeric, 2-digit, long, short, narrow
                hour: "numeric", // numeric, 2-digit
                minute: "numeric", // numeric, 2-digit
                second: "numeric", // numeric, 2-digit
              })}
            </Typography>
          </Grid>
        </Grid>

        <Grid item container justify="center">
          <Grid container direction="row" item xs={10} sm={6} justify="center">
            <TextField
              fullWidth
              value={token}
              placeholder="Token Recibido"
              onChange={(event) => {
                setToken(event.target.value);
              }}
              variant="outlined"
              type="text"
              size="small"
              autoComplete="current-user"
            />
          </Grid>
        </Grid>

        <Grid item container justify="center">
          <Grid
            container
            item
            xs={10}
            sm={6}
            justify="center"
            style={{ paddingBottom: "1vh", paddingTop: "1vh" }}
          >
            <Button
              border={15}
              variant="contained"
              fullWidth
              size="large"
              color="primary"
              className={classes.button}
              onClick={Confirm.bind(this)}
            >
              Confirmar Voto
            </Button>
          </Grid>
        </Grid>
        <Grid item container justify="center">
          <Grid
            container
            item
            xs={10}
            sm={6}
            justify="center"
            style={{ paddingBottom: "1vh", paddingTop: "1vh" }}
          >
            <Button
              border={15}
              variant="contained"
              fullWidth
              size="large"
              color="secondary"
              disabled={!newToken}
              className={classes.button}
              onClick={GenerateToken.bind(this)}
            >
              Generar Token
            </Button>
          </Grid>
        </Grid>
        <Grid item container justify="center" className={classes.subtitle}>
          {timerComponents.length ? timerComponents : <Grid></Grid>}
        </Grid>
        <Grid container item xs={10} justify="flex-end">
          <Snackbar
            message={message}
            handleClose={handleClose}
            open={open}
            severity="error"
          />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default ConfirmVote;
