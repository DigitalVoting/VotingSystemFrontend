import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Banner from "../../components/Banner";
import * as ROUTES from "../../routes/routes";
import {
  Avatar,
  Button,
  Card,
  CardHeader,
  Grid,
  InputAdornment,
  Radio,
  TextField,
  Typography,
} from "@material-ui/core";
import candidatesListService from "../../services/Candidates/candidate-list.service";
import electionsService from "../../services/Elections/elections.service";
import { useHistory, useLocation } from "react-router";
import { Search } from "@material-ui/icons";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: "100vw",
  },
  titleSection: {
    marginTop: theme.spacing(2),
  },
  card: {
    border: "1px solid",
    borderColor: theme.palette.secondary.main,
    borderRadius: "12px",
    margin: theme.spacing(1),
  },
  header: {
    background: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
  },
  button: {},
  iconSearch: {
    color: "#3E5481",
    cursor: "pointer",
  },
  searchField: {
    borderRadius: 24,
    background: "#F4F5F7",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}));

const Vote = () => {
  const classes = useStyles();
  const history = useHistory();
  const location = useLocation();
  const [election, setElection] = useState({});
  const [candidatesLists, setCandidatesLists] = useState([]);
  const [selectedList, setSelectedList] = useState();

  const [searchText, setSearchText] = useState("");

  const GetCandidatesListsCurrent = () => {
    candidatesListService
      .getCandidatesListsCurrent()
      .then((response) => {
        if (response.data) {
          setSelectedList(response.data[0]);
          setCandidatesLists(response.data);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetElection = () => {
    electionsService
      .activeElection()
      .then((response) => {
        if (response.data) {
          if (response.data.stage !== "vote") history.push(ROUTES.ELECCION);
          else setElection(response.data);
        } else {
          console.log("No data");
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const onKeyUp = (event) => {
    if (event.charCode === 13) {
      SearchText();
    }
  };

  useEffect(() => {
    if (location.state && location.state.election) {
      GetCandidatesListsCurrent();
      GetElection();
    } else {
      history.push(ROUTES.ELECCION);
    }
  }, []);

  const ConfirmVote = () => {
    history.push({
      pathname: ROUTES.CONFIRMAR_VOTACION,
      state: { candidatesList: selectedList, election: election },
    });
  };

  const SearchText = () => {
    candidatesListService
      .findCandidatesListsCurrent(searchText)
      .then((response) => {
        if (response.data) {
          setCandidatesLists(response.data);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleChange = (event) => {
    for (const candidatesList of candidatesLists) {
      if (candidatesList.id.toString() === event.target.value) {
        setSelectedList(candidatesList);
        break;
      }
    }
  };

  const options = candidatesLists.map((candidatesList) => {
    return (
      <Grid key={candidatesList.id}>
        <Card className={classes.card}>
          <CardHeader
            disableTypography
            title={
              <Grid
                item
                container
                direction="row"
                align="center"
                className={classes.title}
              >
                <Grid item xs={2} container justify="flex-start">
                  <Avatar
                    alt={candidatesList.name}
                    src={candidatesList.logo}
                    className={classes.lista}
                  />
                </Grid>
                <Grid
                  item
                  xs={8}
                  container
                  direction="column"
                  justify="center"
                  align="center"
                >
                  <Grid item>
                    <Typography
                      variant="body2"
                      align="left"
                      style={{ alignItems: "center" }}
                    >
                      {candidatesList.name}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid
                  item
                  xs={2}
                  container
                  direction="column"
                  justify="center"
                  align="center"
                >
                  <Grid item>
                    <Radio
                      checked={selectedList.id === candidatesList.id}
                      onChange={handleChange}
                      value={candidatesList.id}
                      color="primary"
                    />
                  </Grid>
                </Grid>
              </Grid>
            }
            className={classes.header}
          />
        </Card>
      </Grid>
    );
  });

  return (
    <Grid container justify="center">
      <Grid
        container
        direction="column"
        justify="center"
        align="center"
        className={classes.root}
        spacing={3}
      >
        <Grid item>
          <Banner
            keyID={1}
            titleBanner={election.title}
            imageBanner={election.banner}
            imageTitle={election.title}
          />
        </Grid>

        <Grid item container justify="center" className={classes.content}>
          <Grid
            item
            container
            xs={10}
            justify="center"
            className={classes.searchContent}
          >
            <TextField
              className={classes.searchField}
              fullWidth
              value={searchText}
              placeholder="Buscar"
              onChange={(event) => {
                setSearchText(event.target.value);
              }}
              variant="outlined"
              type="text"
              size="small"
              autoComplete="current-user"
              inputProps={{ maxLength: 20 }}
              InputProps={{
                endAdornment: (
                  <InputAdornment
                    position="end"
                    className={classes.iconSearch}
                    onClick={SearchText}
                  >
                    <Search />
                  </InputAdornment>
                ),
              }}
              onKeyPress={onKeyUp}
            />
          </Grid>
        </Grid>

        {options[0] ? options : <Grid></Grid>}

        <Grid item>
          <Button
            border={15}
            variant="contained"
            fullWidth
            size="large"
            color="primary"
            className={classes.button}
            onClick={ConfirmVote.bind(this)}
          >
            Votar
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};
export default Vote;
