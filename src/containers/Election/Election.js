import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Banner from "../../components/Banner";
import Snackbar from "../../components/Snackbar";
import * as ROUTES from "../../routes/routes";
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Typography,
} from "@material-ui/core";
import electionsService from "../../services/Elections/elections.service";
import { useHistory } from "react-router";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: "100vw",
  },
  titleSection: {
    marginTop: theme.spacing(2),
  },
  card: {
    minWidth: 325,
    maxWidth: 325,
    border: "1px solid",
    borderColor: theme.palette.secondary.main,
    borderRadius: "12px",
  },
  header: {
    background: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
  },
  content: {
    alignContent: "center",
    justifyContent: "center",
  },
}));

const Election = () => {
  const classes = useStyles();
  const history = useHistory();
  const [election, setElection] = useState({});

  const user = JSON.parse(localStorage.getItem("user"));
  const [error, setError] = React.useState(false);
  const [errorText, setErrorText] = useState("");

  const [online, setOnline] = useState(navigator.onLine);

  window.addEventListener("offline", function () {
    if (online) {
      setOnline(false);
      localStorage.setItem("election", JSON.stringify(election));
    }
  });

  window.addEventListener("online", function () {
    if (!online) {
      localStorage.removeItem("election");
      setOnline(true);
      GetElection();
    }
  });

  const GetElection = () => {
    electionsService
      .activeElection()
      .then((response) => {
        if (response.data) {
          setElection(response.data);
        } else {
          console.log("No data");
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    if (navigator.onLine) {
      GetElection();
      const electionSaved = JSON.parse(localStorage.getItem("election"));
      if (electionSaved) {
        localStorage.removeItem("election");
      }
    } else {
      const electionSaved = JSON.parse(localStorage.getItem("election"));
      if (electionSaved) {
        setElection(electionSaved);
      }
    }
  }, []);

  const Ruta = (stage) => {
    if (online) {
      if (stage == "inscriptions") history.push(ROUTES.REGISTRO_LISTA);
      else if (stage == "actualization") history.push(ROUTES.PERFIL);
      else if (stage == "vote") {
        electionsService
          .hasVoted(user.id, election.id)
          .then((response) => {
            if (!response.data) {
              history.push({
                pathname: ROUTES.VOTACION,
                state: { election: election },
              });
            } else {
              setErrorText("Ya ha realizado su votación");
              setError(true);
            }
          })
          .catch((e) => {
            console.log(e);
          });
      } else
        history.push({
          pathname: ROUTES.RESULTADOS,
          state: { election: election },
        });
    } else {
      setErrorText("No cuenta con conexión a internet");
      setError(true);
    }
  };

  const handleCloseError = () => {
    setError(false);
  };

  return (
    <Grid container justify="center">
      {election.title ? (
        <Grid>
          <Grid
            container
            direction="column"
            justify="center"
            align="center"
            className={classes.root}
            spacing={3}
          >
            <Grid item>
              <Banner
                keyID={1}
                titleBanner={election.title}
                imageBanner={election.banner}
                imageTitle={election.title}
              />
            </Grid>

            <Grid item>
              <Card className={classes.card}>
                <CardHeader
                  disableTypography
                  title={
                    <Grid>
                      <Typography
                        variant="subtitle1"
                        align="justify"
                        style={{ margin: "5px" }}
                      >
                        {election.information}
                      </Typography>
                      <Typography
                        variant="subtitle1"
                        align="justify"
                        style={{ margin: "5px" }}
                      >
                        {"Fecha Inicio: " + election.startDate}
                      </Typography>
                      <Typography
                        variant="subtitle1"
                        align="justify"
                        style={{ margin: "5px" }}
                      >
                        {"Fecha Fin: " + election.endDate}
                      </Typography>
                    </Grid>
                  }
                  className={classes.header}
                />
              </Card>
            </Grid>
            {election.stage != "results" ? (
              <Grid item>
                <Button
                  border={15}
                  variant="contained"
                  fullWidth
                  size="large"
                  disabled
                  className={classes.button}
                >
                  {election.stage == "waiting"
                    ? "Registro de Listas"
                    : election.stage == "inscriptions"
                    ? "Actualizar Información"
                    : election.stage == "actualization"
                    ? "Votar"
                    : "Resultados"}
                </Button>
              </Grid>
            ) : null}
            {election.stage != "waiting" ? (
              <Grid item>
                <Button
                  border={15}
                  variant="contained"
                  fullWidth
                  size="large"
                  color="primary"
                  className={classes.button}
                  onClick={Ruta.bind(this, election.stage)}
                >
                  {election.stage == "inscriptions"
                    ? "Registro de Listas"
                    : election.stage == "actualization"
                    ? "Actualizar Información"
                    : election.stage == "vote"
                    ? "Votar"
                    : "Resultados"}
                </Button>
              </Grid>
            ) : null}
          </Grid>
          <Grid container item xs={10} justify="flex-end">
            <Snackbar
              message={errorText}
              handleClose={handleCloseError}
              open={error}
              severity="error"
            />
          </Grid>
        </Grid>
      ) : (
        <Grid item className={classes.notification}>
          <Card className={classes.root}>
            <Grid container direction="row">
              <Grid item container direction="column">
                <CardContent className={classes.content}>
                  <Typography variant="h6" align="center">
                    {
                      "No se cuenta con información disponible en estos momentos"
                    }
                  </Typography>
                </CardContent>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      )}
    </Grid>
  );
};
export default Election;
