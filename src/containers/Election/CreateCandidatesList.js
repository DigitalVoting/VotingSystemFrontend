import {
  Avatar,
  Button,
  Dialog,
  DialogTitle,
  Fade,
  FormControl,
  Grid,
  InputAdornment,
  InputLabel,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  makeStyles,
  MenuItem,
  Modal,
  Select,
  TextField,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Backdrop from "@material-ui/core/Backdrop";
import { useHistory } from "react-router";
import {
  Add,
  ArrowBackIos,
  Check,
  Create,
  Person,
  Search,
} from "@material-ui/icons";
import candidatesListService from "../../services/Candidates/candidate-list.service";
import proposalsService from "../../services/Candidates/proposals.service";
import usersService from "../../services/Users/users.service";
import notificationsService from "../../services/Users/notifications.service";
import electionsService from "../../services/Elections/elections.service";
import Snackbar from "../../components/Snackbar";
import * as ROUTES from "../../routes/routes";

const useStyles = makeStyles((theme) => ({
  titleText: {
    color: "#2E3E5C",
  },
  subtitleText: {
    color: "#9FA5C0",
  },
  button: {},
  description: {
    maxWidth: "100vw",
    minHeight: "60vh",
    padding: theme.spacing(2),
  },
  content: {
    minHeight: "94vh",
    maxWidth: "100vw",
    marginLeft: 0,
    marginTop: "-5vh",
    paddingTop: "5vh",
  },
  avatar: {
    width: theme.spacing(20),
    height: theme.spacing(20),
  },
  backButton: {
    cursor: "pointer",
    top: 20,
    left: 20,
    position: "fixed",
    backgroundColor: "#000000",
    opacity: "0.2",
    borderRadius: "24px",
  },
  editButton: {
    cursor: "pointer",
    top: 20,
    right: 20,
    position: "fixed",
    backgroundColor: "#851610",
    opacity: "0.2",
    borderRadius: "24px",
  },
  editInfo: {
    cursor: "pointer",
    backgroundColor: "#851610",
    opacity: "0.2",
    borderRadius: "24px",
  },
  icon: {
    color: "#FFFFFF",
    display: "flex",
    justifyContent: "center",
  },
  add: {
    color: "#3E5481",
    cursor: "pointer",
  },
  lista: {
    width: theme.spacing(5),
    height: theme.spacing(5),
  },
  spaceDivider: {
    backgroundColor: "#D0DBEA",
    height: "2px",
    padding: "0px !important",
    width: "90vw",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: "10px",
    maxWidth: "90vw",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 2, 3),
  },
  iconSearch: {
    color: "#3E5481",
    cursor: "pointer",
  },
  avatarList: {
    color: theme.palette.primary.main,
    background: theme.palette.secondary.contrastText,
    border: "1px solid",
  },
  searchField: {
    borderRadius: 24,
    background: "#F4F5F7",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}));

const CreateCandidatesList = () => {
  const classes = useStyles();
  const history = useHistory();

  const user = JSON.parse(localStorage.getItem("user"));
  const [candidatesList, setCandidatesList] = useState();
  const [candidates, setCandidates] = useState([]);
  const [activeUsers, setActiveUsers] = useState([]);
  const [proposals, setProposals] = useState([]);
  const [election, setElection] = useState();
  const [changed, setChanged] = useState(false);
  const [candidateStatus, setCandidateStatus] = useState("");

  const [acceptedCandidates, setAcceptedCandidates] = useState([]);
  const [pendingCandidates, setPendingCandidates] = useState([]);

  const [open, setOpen] = React.useState(false);
  const [openList, setOpenList] = React.useState(false);
  const [error, setError] = React.useState(false);
  const [edit, setEdit] = useState(false);
  const [editCandidate, setEditCandidate] = useState(false);
  const [informationModal, setInformationModal] = useState();
  const [searchText, setSearchText] = useState("");
  const [errorText, setErrorText] = useState("");

  const GetCandidates = (list) => {
    candidatesListService
      .getCandidatesOfCandidatesList(list.id)
      .then((response) => {
        if (response.data) {
          setCandidates(response.data);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetCandidatesStatus = (listId) => {
    notificationsService
      .listCandidatesStatus(listId)
      .then((response) => {
        if (response.data) {
          setCandidateStatus(response.data.status);
          setAcceptedCandidates(response.data.acceptedCandidates);
          setPendingCandidates(response.data.pendingCandidates);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetElection = () => {
    electionsService
      .activeElection()
      .then((response) => {
        if (response.data) {
          if (response.data.stage !== "inscriptions")
            history.push(ROUTES.ELECCION);
          else setElection(response.data);
        } else {
          console.log("No data");
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    electionsService
      .getStage(user.id)
      .then((response) => {
        if (response.data) {
          if (response.data !== "inscriptions") history.push(ROUTES.ELECCION);
          else {
            usersService
              .getCurrentList(user.id)
              .then((res) => {
                if (res.data.message) {
                  let newCandidatesList = {
                    id: 0,
                    name: "",
                    description: "",
                    logo: "",
                    status: "NEW",
                    proposals: [],
                  };
                  setCandidatesList(newCandidatesList);
                  setEdit(true);
                } else {
                  setCandidatesList(res.data);
                  setProposals(res.data.proposals);
                  GetCandidates(res.data);
                  GetCandidatesStatus(res.data.id);
                }
              })
              .catch((e) => {
                console.log(e);
              });
            GetElection();
          }
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  const Back = () => {
    history.goBack();
  };

  const EditButton = () => {
    setEdit(true);
  };

  const ConfirmButton = () => {
    if (changed || candidateStatus === "ACCEPTED") {
      if (candidatesList.status == "PEN") {
        if (candidates.length < 6) {
          setErrorText(
            "No se cuenta con los integrantes necesarios para enviar la solicitud de registro"
          );
          setError(true);
        } else {
          let ready = true;
          let isAdded = false;
          for (let index = 0; index < candidates.length; index++) {
            if (GetStatus(candidates[index]) !== "Solicitud: Aceptado") {
              ready = false;
              break;
            }
            if (candidates[index].id === user.id) {
              isAdded = true;
            }
          }
          if (ready && isAdded) {
            let proposalsAux = [];
            proposalsAux = [...candidatesList.proposals];
            proposalsService.updateProposals(
              candidatesList.id,
              user.id,
              proposalsAux
            );
            candidatesListService.updateCandidateList(
              candidatesList.id,
              candidatesList.name,
              candidatesList.description,
              candidatesList.logo,
              "SOL",
              user.id
            );
            let aux = { ...candidatesList };
            aux.status = "SOL";
            setCandidatesList(aux);
            setEdit(false);
          } else {
            if (!ready) {
              setErrorText("Hay solicitudes de integrantes pendientes");
              let proposalsAux = [];
              proposalsAux = [...candidatesList.proposals];
              proposalsService.updateProposals(
                candidatesList.id,
                user.id,
                proposalsAux
              );
              candidatesListService.updateCandidateList(
                candidatesList.id,
                candidatesList.name,
                candidatesList.description,
                candidatesList.logo,
                candidatesList.status,
                user.id,
                "sol"
              );
            } else {
              setErrorText("No se ha añadido como integrante de la lista");
            }
            setError(true);
          }
        }
      } else {
        if (candidates.length < 6) {
          setErrorText(
            "No se cuenta con los integrantes necesarios para enviar las solicitudes"
          );
          setError(true);
        } else {
          let isAdded = false;
          for (const candidate of candidates) {
            if (candidate.id === user.id) {
              isAdded = true;
            }
          }
          if (isAdded) {
            candidatesListService
              .saveCandidatesList(
                candidatesList,
                candidates,
                user.id,
                "PEN",
                election.id
              )
              .then((response) => {
                if (response.data) {
                  if (proposals.length > 0) {
                    usersService
                      .getCurrentList(user.id)
                      .then((res) => {
                        if (res.data) {
                          proposalsService
                            .updateProposals(res.data.id, user.id, proposals)
                            .then((prop) => {
                              let aux = { ...candidatesList };
                              aux.status = "PEN";
                              setCandidatesList(aux);
                              setErrorText(prop.data.message);
                              setError(true);
                              setEdit(false);
                            })
                            .catch((e) => {
                              console.log(e);
                            });
                        }
                      })
                      .catch((e) => {
                        console.log(e);
                      });
                  } else {
                    let aux = { ...candidatesList };
                    aux.status = "PEN";
                    user.roles.push("ROLE_LIST_ADMIN");
                    localStorage.setItem("user", user);
                    setCandidatesList(aux);
                    setErrorText(response.data);
                    setError(true);
                    setEdit(false);
                  }
                }
              })
              .catch((e) => {
                console.log(e);
              });
          } else {
            setErrorText("No se ha añadido como integrante de la lista");
            setError(true);
          }
        }
      }
    } else {
      setErrorText("No se han realizado cambios");
      setError(true);
    }
  };

  const handleOpen = (information) => {
    if (information.type == "candidate") {
      setEditCandidate(true);
    } else {
      information.type = "proposal";
    }
    setInformationModal(information);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setEditCandidate(false);
  };

  const handleCloseList = () => {
    setOpenList(false);
  };

  const handleCloseError = () => {
    setError(false);
  };

  const handleChange = (event) => {
    let infAux = { ...informationModal };
    infAux.user.position = event.target.value;
    setInformationModal(infAux);
  };

  const GetStatus = (candidate) => {
    for (let index = 0; index < acceptedCandidates.length; index++) {
      if (acceptedCandidates[index] == candidate.id)
        return "Solicitud: Aceptado";
    }
    return "Solicitud: Pendiente";
  };

  const SaveInformation = () => {
    if (informationModal.type == "proposal") {
      if (informationModal.name !== "" && informationModal.description !== "") {
        let newProfile = { ...candidatesList };
        let exists = false;
        for (let index = 0; index < newProfile.proposals.length; index++) {
          if (newProfile.proposals[index].id === informationModal.id) {
            exists = true;
            newProfile.proposals[index] = informationModal;
            setProposals(newProfile.proposals);
            setCandidatesList(newProfile);
          }
        }
        if (!exists) {
          newProfile.proposals.push(informationModal);
          setProposals(newProfile.proposals);
          setCandidatesList(newProfile);
        }
        setChanged(true);
      } else {
        setErrorText("No se completaron los campos requeridos");
        setError(true);
      }
    } else {
      if (informationModal.user) {
        let candidatesAux = [...candidates];
        let exists = false;
        for (let index = 0; index < candidatesAux.length; index++) {
          if (candidatesAux[index].id === informationModal.user.id) {
            if (
              pendingCandidates.includes(candidatesAux[index].id) ||
              acceptedCandidates.includes(candidatesAux[index].id)
            ) {
              exists = true;
              setErrorText(
                "Ya se ha asignado el usuario a un cargo y/o se encuentra pendiente su respuesta"
              );
              setError(true);
            } else {
              exists = true;
              candidatesAux[index] = informationModal.user;
              if (editCandidate) {
                setCandidates(candidatesAux);
              } else {
                setErrorText("El usuario seleccionado ya se encuentra añadido");
                setError(true);
              }
            }
          }
          if (
            candidatesAux[index].position === informationModal.user.position
          ) {
            if (
              pendingCandidates.includes(candidatesAux[index].id) ||
              acceptedCandidates.includes(candidatesAux[index].id)
            ) {
              exists = true;
              setErrorText(
                "Ya se ha asignado dicho cargo y/o se encuentra pendiente la respuesta"
              );
              setError(true);
            } else {
              exists = true;
              candidatesAux[index] = informationModal.user;
              setCandidates(candidatesAux);
            }
          }
        }
        if (!exists) {
          candidatesAux.push(informationModal.user);
          setCandidates(candidatesAux);
        }
        setChanged(true);
      } else {
        setErrorText("No se ha seleccionado un usuario");
        setError(true);
      }
    }
    handleClose();
  };

  const AddInformation = (type) => {
    let information = {
      id: 0,
      name: "",
      description: "",
      type: type,
      user: null,
    };
    setInformationModal(information);
    setOpen(true);
  };

  const onKeyUp = (event) => {
    if (event.charCode === 13) {
      SearchText();
    }
  };

  const SearchText = () => {
    usersService
      .findActiveElectors(searchText)
      .then((response) => {
        if (response.data) {
          setActiveUsers(response.data);
          setOpenList(true);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleListItemClick = (item) => {
    let infAux = { ...informationModal };
    infAux.user = item;
    setInformationModal(infAux);
    handleCloseList();
  };

  const candidatesContent = candidates.map((candidate) => {
    return (
      <Grid
        key={candidate.id}
        item
        container
        direction="row"
        align="center"
        className={classes.title}
      >
        <Grid item xs={2} container justify="flex-start">
          <Avatar
            alt={candidate.names}
            src={candidate.photo}
            className={classes.lista}
          />
        </Grid>
        <Grid
          item
          xs={9}
          container
          direction="column"
          justify="center"
          align="center"
        >
          <Grid item>
            <Typography
              variant="body2"
              align="left"
              style={{ color: "#3E5481", alignItems: "center" }}
            >
              {candidate.names +
                " " +
                candidate.lastNames +
                " - " +
                candidate.position}
            </Typography>
          </Grid>
          {candidatesList.status == "PEN" ? (
            <Grid item>
              <Typography
                variant="body2"
                align="left"
                style={{ color: "#3E5481", alignItems: "center" }}
              >
                {GetStatus(candidate)}
              </Typography>
            </Grid>
          ) : (
            <Grid></Grid>
          )}
        </Grid>
        {edit ? (
          <Grid xs={1} item container direction="column">
            <Grid
              item
              container
              className={classes.editInfo}
              onClick={handleOpen.bind(this, {
                type: "candidate",
                user: candidate,
              })}
            >
              <Create className={classes.icon} />
            </Grid>
          </Grid>
        ) : null}
      </Grid>
    );
  });

  const proposalsContent = proposals.map((proposal) => {
    return (
      <Grid key={proposal.id} item container className={classes.title}>
        <Grid item container xs={edit ? 11 : 12}>
          <Typography variant="subtitle1" className={classes.titleText}>
            {proposal.name}
          </Typography>
          <Typography
            variant="subtitle1"
            align="justify"
            className={classes.subtitleText}
          >
            {proposal.description}
          </Typography>
        </Grid>
        {edit ? (
          <Grid xs={1} item container direction="column">
            <Grid
              item
              container
              className={classes.editInfo}
              onClick={handleOpen.bind(this, proposal)}
            >
              <Create className={classes.icon} />
            </Grid>
          </Grid>
        ) : null}
      </Grid>
    );
  });

  return (
    <Grid>
      {candidatesList ? (
        <Grid
          container
          direction="column"
          align="center"
          className={classes.content}
          spacing={2}
        >
          <Grid item className={classes.backButton} onClick={Back.bind(this)}>
            <ArrowBackIos className={classes.icon} />
          </Grid>
          {candidatesList.status != "ACT" &&
          candidatesList.status != "SOL" &&
          user.roles.includes("ROLE_LIST_ADMIN") ? (
            !edit ? (
              <Grid
                item
                className={classes.editButton}
                onClick={EditButton.bind(this)}
              >
                <Create className={classes.icon} />
              </Grid>
            ) : (
              <Grid
                item
                className={classes.editButton}
                onClick={ConfirmButton.bind(this)}
              >
                <Check className={classes.icon} />
              </Grid>
            )
          ) : null}
          {candidatesList.id == 0 ? (
            !edit ? (
              <Grid
                item
                className={classes.editButton}
                onClick={EditButton.bind(this)}
              >
                <Create className={classes.icon} />
              </Grid>
            ) : (
              <Grid
                item
                className={classes.editButton}
                onClick={ConfirmButton.bind(this)}
              >
                <Check className={classes.icon} />
              </Grid>
            )
          ) : null}
          <Grid item container justify="center">
            <Avatar
              alt={candidatesList.name}
              src={candidatesList.logo}
              className={classes.avatar}
            />
          </Grid>
          <Grid item container justify="center">
            {!edit ? (
              <Grid
                item
                container
                className={classes.title}
                align="center"
                justify="center"
              >
                <Typography variant="h6" className={classes.titleText}>
                  {candidatesList.name}
                </Typography>
              </Grid>
            ) : (
              <Grid item container className={classes.title}>
                <TextField
                  fullWidth
                  value={candidatesList.name}
                  placeholder="Nombre"
                  onChange={(event) => {
                    let presAux = { ...candidatesList };
                    presAux.name = event.target.value;
                    setChanged(true);
                    setCandidatesList(presAux);
                  }}
                  variant="outlined"
                  type="text"
                  size="small"
                />
              </Grid>
            )}
          </Grid>
          <Grid
            container
            direction="column"
            alignItems="center"
            spacing={1}
            className={classes.description}
          >
            <Grid item container className={classes.title}>
              <Typography variant="h6" className={classes.titleText}>
                Presentación
              </Typography>
            </Grid>
            <Grid item container className={classes.title}>
              {edit ? (
                <TextField
                  fullWidth
                  value={candidatesList.description}
                  placeholder="Presentación"
                  onChange={(event) => {
                    let presAux = { ...candidatesList };
                    presAux.description = event.target.value;
                    setChanged(true);
                    setCandidatesList(presAux);
                  }}
                  variant="outlined"
                  type="text"
                  size="small"
                  multiline
                  rows={4}
                />
              ) : (
                <Typography
                  variant="subtitle1"
                  align="justify"
                  className={classes.subtitleText}
                >
                  {candidatesList.description
                    ? candidatesList.description
                    : "No ha registrado su presentación"}
                </Typography>
              )}
            </Grid>
            <Grid item className={classes.spaceDivider} />
            <Grid item container className={classes.title}>
              <Grid xs={10} item container>
                <Typography variant="h6" className={classes.titleText}>
                  Integrantes
                </Typography>
              </Grid>
              {edit ? (
                <Grid
                  xs={1}
                  item
                  container
                  direction="column"
                  justify="center"
                  align="center"
                >
                  <Add
                    className={classes.add}
                    onClick={AddInformation.bind(this, "candidate")}
                  />
                </Grid>
              ) : null}
            </Grid>
            {candidatesContent[0] ? (
              candidatesContent
            ) : (
              <Grid>
                <Typography
                  variant="subtitle1"
                  className={classes.subtitleText}
                >
                  No han registrado candidatos
                </Typography>
              </Grid>
            )}
            <Grid item className={classes.spaceDivider} />
            <Grid item container className={classes.title}>
              <Grid xs={10} item container>
                <Typography variant="h6" className={classes.titleText}>
                  Propuestas
                </Typography>
              </Grid>
              {edit ? (
                <Grid
                  xs={1}
                  item
                  container
                  direction="column"
                  justify="center"
                  align="center"
                >
                  <Add
                    className={classes.add}
                    onClick={AddInformation.bind(this, "proposal")}
                  />
                </Grid>
              ) : null}
            </Grid>
            {proposalsContent[0] ? (
              proposalsContent
            ) : (
              <Grid>
                <Typography
                  variant="subtitle1"
                  className={classes.subtitleText}
                >
                  No han registrado propuestas
                </Typography>
              </Grid>
            )}
          </Grid>
        </Grid>
      ) : (
        <Grid></Grid>
      )}
      <Modal
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        {informationModal ? (
          <Fade in={open}>
            {informationModal.type == "proposal" ? (
              <Grid container className={classes.paper}>
                <Grid item container style={{ margin: "1vh" }}>
                  <Grid
                    xs={4}
                    item
                    container
                    style={{ alignContent: "center" }}
                  >
                    <Typography variant="h6" className={classes.titleText}>
                      Título
                    </Typography>
                  </Grid>
                  <Grid container direction="row" item xs={8} justify="center">
                    <TextField
                      fullWidth
                      value={informationModal.name}
                      placeholder="Título"
                      onChange={(event) => {
                        let infAux = { ...informationModal };
                        infAux.name = event.target.value;
                        setInformationModal(infAux);
                      }}
                      variant="outlined"
                      type="text"
                      size="small"
                    />
                  </Grid>
                </Grid>
                <Grid item container style={{ margin: "1vh" }}>
                  <Grid item container style={{ alignContent: "center" }}>
                    <Typography variant="h6" className={classes.titleText}>
                      Descripcion
                    </Typography>
                  </Grid>
                </Grid>
                <Grid item container style={{ margin: "1vh" }}>
                  <Grid container direction="row" item justify="center">
                    <TextField
                      fullWidth
                      value={informationModal.description}
                      placeholder="Descripción"
                      onChange={(event) => {
                        let infAux = { ...informationModal };
                        infAux.description = event.target.value;
                        setInformationModal(infAux);
                      }}
                      variant="outlined"
                      type="text"
                      size="small"
                      multiline
                      rows={4}
                    />
                  </Grid>
                </Grid>
                <Grid
                  container
                  item
                  xs={12}
                  sm={4}
                  justify="center"
                  style={{ paddingBottom: "3vh", paddingTop: "3vh" }}
                >
                  <Button
                    border={15}
                    variant="contained"
                    fullWidth
                    size="large"
                    color="secondary"
                    className={classes.button}
                    onClick={SaveInformation}
                  >
                    Confirmar Cambios
                  </Button>
                </Grid>
              </Grid>
            ) : (
              <Grid container className={classes.paper}>
                <Grid item container style={{ margin: "1vh" }}>
                  {editCandidate ? (
                    <Grid></Grid>
                  ) : (
                    <Grid item container justify="center">
                      <Grid
                        item
                        container
                        xs={10}
                        justify="center"
                        className={classes.searchContent}
                      >
                        <TextField
                          className={classes.searchField}
                          fullWidth
                          value={searchText}
                          placeholder="Buscar"
                          onChange={(event) => {
                            setSearchText(event.target.value);
                          }}
                          variant="outlined"
                          type="text"
                          size="small"
                          autoComplete="current-user"
                          inputProps={{ maxLength: 20 }}
                          InputProps={{
                            endAdornment: (
                              <InputAdornment
                                position="end"
                                className={classes.iconSearch}
                                onClick={SearchText}
                              >
                                <Search />
                              </InputAdornment>
                            ),
                          }}
                          onKeyPress={onKeyUp}
                        />
                      </Grid>
                    </Grid>
                  )}
                  <Grid item container justify="center">
                    <Avatar
                      alt={informationModal.name}
                      src={
                        informationModal.user
                          ? informationModal.user.photo
                          : null
                      }
                      className={classes.avatar}
                    />
                  </Grid>
                  <Grid
                    item
                    container
                    style={{ alignContent: "center", justifyContent: "center" }}
                  >
                    <Typography
                      variant="subtitle3"
                      className={classes.titleText}
                      align="center"
                    >
                      {informationModal.user
                        ? "Candidato Seleccionado: "
                        : "Busque un Usuario"}
                    </Typography>
                  </Grid>
                  <Grid
                    item
                    container
                    style={{ alignContent: "center", justifyContent: "center" }}
                  >
                    <Typography
                      variant="subtitle3"
                      className={classes.titleText}
                      align="center"
                    >
                      {informationModal.user
                        ? informationModal.user.names +
                          " " +
                          informationModal.user.lastNames
                        : null}
                    </Typography>
                  </Grid>
                  {informationModal.user ? (
                    <Grid
                      item
                      container
                      style={{
                        alignContent: "center",
                        justifyContent: "center",
                      }}
                    >
                      <FormControl className={classes.formControl}>
                        <InputLabel id="select-label">Cargo</InputLabel>
                        <Select
                          id="select"
                          value={
                            informationModal.user
                              ? informationModal.user.position
                              : null
                          }
                          onChange={handleChange}
                          autoWidth={false}
                        >
                          <MenuItem value={"Decano"}>Decano</MenuItem>
                          <MenuItem value={"Vicedecano"}>Vicedecano</MenuItem>
                          <MenuItem value={"Director Secretario"}>
                            Director Secretario
                          </MenuItem>
                          <MenuItem value={"Director Tesorero"}>
                            Director Tesorero
                          </MenuItem>
                          <MenuItem value={"Director Prosecretario"}>
                            Director Prosecretario
                          </MenuItem>
                          <MenuItem value={"Director Protesorero"}>
                            Director Protesorero
                          </MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                  ) : (
                    <Grid></Grid>
                  )}
                </Grid>
                <Grid
                  container
                  item
                  xs={12}
                  sm={4}
                  justify="center"
                  style={{ paddingBottom: "3vh", paddingTop: "3vh" }}
                >
                  <Button
                    border={15}
                    variant="contained"
                    fullWidth
                    size="large"
                    color="secondary"
                    className={classes.button}
                    onClick={SaveInformation}
                  >
                    {editCandidate ? "Guardar Cambios" : "Agregar Integrante"}
                  </Button>
                </Grid>
                <Dialog
                  onClose={handleCloseList}
                  aria-labelledby="simple-dialog-title"
                  open={openList}
                >
                  <DialogTitle
                    id="simple-dialog-title"
                    className={classes.title}
                  >
                    Usuarios Activos
                  </DialogTitle>
                  <List>
                    {activeUsers.map((item) => (
                      <ListItem
                        button
                        onClick={handleListItemClick.bind(this, item)}
                        key={item.id}
                      >
                        <ListItemAvatar>
                          <Avatar className={classes.avatarList}>
                            <Person />
                          </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                          primary={item.names + " " + item.lastNames}
                        />
                      </ListItem>
                    ))}
                  </List>
                </Dialog>
              </Grid>
            )}
          </Fade>
        ) : (
          <Grid></Grid>
        )}
      </Modal>
      <Grid container item xs={10} justify="flex-end">
        <Snackbar
          message={errorText}
          handleClose={handleCloseError}
          open={error}
          severity="error"
        />
      </Grid>
    </Grid>
  );
};

export default CreateCandidatesList;
