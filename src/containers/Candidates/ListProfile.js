import {
  Avatar,
  Button,
  Fade,
  Grid,
  makeStyles,
  Modal,
  TextField,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Backdrop from "@material-ui/core/Backdrop";
import { useHistory } from "react-router";
import { Add, ArrowBackIos, Check, Create } from "@material-ui/icons";
import candidatesListService from "../../services/Candidates/candidate-list.service";
import proposalsService from "../../services/Candidates/proposals.service";
import userService from "../../services/Users/users.service";
import * as ROUTES from "../../routes/routes";
import electionsService from "../../services/Elections/elections.service";

const useStyles = makeStyles((theme) => ({
  titleText: {
    color: "#2E3E5C",
  },
  subtitleText: {
    color: "#9FA5C0",
  },
  button: {},
  description: {
    maxWidth: "100vw",
    minHeight: "60vh",
    padding: theme.spacing(2),
  },
  content: {
    minHeight: "94vh",
    maxWidth: "100vw",
    marginLeft: 0,
    marginTop: "-5vh",
    paddingTop: "5vh",
  },
  avatar: {
    width: theme.spacing(20),
    height: theme.spacing(20),
  },
  backButton: {
    cursor: "pointer",
    top: 20,
    left: 20,
    position: "fixed",
    backgroundColor: "#000000",
    opacity: "0.2",
    borderRadius: "24px",
  },
  editButton: {
    cursor: "pointer",
    top: 20,
    right: 20,
    position: "fixed",
    backgroundColor: "#851610",
    opacity: "0.2",
    borderRadius: "24px",
  },
  editInfo: {
    cursor: "pointer",
    backgroundColor: "#851610",
    opacity: "0.2",
    borderRadius: "24px",
  },
  icon: {
    color: "#FFFFFF",
    display: "flex",
    justifyContent: "center",
  },
  add: {
    color: "#3E5481",
    cursor: "pointer",
  },
  lista: {
    width: theme.spacing(5),
    height: theme.spacing(5),
  },
  spaceDivider: {
    backgroundColor: "#D0DBEA",
    height: "2px",
    padding: "0px !important",
    width: "90vw",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: "10px",
    maxWidth: "90vw",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 2, 3),
  },
}));

const ListProfile = () => {
  const classes = useStyles();
  const history = useHistory();

  const user = JSON.parse(localStorage.getItem("user"));
  const [candidatesList, setCandidatesList] = useState();
  const [candidates, setCandidates] = useState([]);
  const [proposals, setProposals] = useState([]);
  const [changed, setChanged] = useState(false);

  const [open, setOpen] = React.useState(false);
  const [edit, setEdit] = useState(false);
  const [informationModal, setInformationModal] = useState();

  const GetCandidates = (list) => {
    candidatesListService
      .getCandidatesOfCandidatesList(list.id)
      .then((response) => {
        if (response.data) {
          setCandidates(response.data);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    electionsService
      .getStage(user.id)
      .then((response) => {
        if (response.data) {
          if (response.data !== "actualization") history.push(ROUTES.PERFIL);
          else {
            userService
              .getCurrentList(user.id)
              .then((res) => {
                if (res.data) {
                  setCandidatesList(res.data);
                  setProposals(res.data.proposals);
                  GetCandidates(res.data);
                }
              })
              .catch((e) => {
                console.log(e);
              });
          }
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  const Back = () => {
    history.goBack();
  };

  const EditButton = () => {
    setEdit(true);
  };

  const ConfirmButton = () => {
    if (changed) {
      let proposalsAux = [];
      proposalsAux = [...candidatesList.proposals];
      proposalsService.updateProposals(
        candidatesList.id,
        user.id,
        proposalsAux
      );
      candidatesListService.updateCandidateList(
        candidatesList.id,
        candidatesList.name,
        candidatesList.description,
        candidatesList.logo,
        candidatesList.status,
        user.id
      );
    }
    setEdit(false);
  };

  const handleOpen = (information) => {
    setInformationModal(information);
    setOpen(true);
  };

  const handleClose = () => {
    console.log(informationModal);
    setOpen(false);
  };

  const SaveInformation = () => {
    let newProfile = { ...candidatesList };

    let exists = false;
    for (let index = 0; index < newProfile.proposals.length; index++) {
      if (newProfile.proposals[index].id === informationModal.id) {
        exists = true;
        newProfile.proposals[index] = informationModal;
        setCandidatesList(newProfile);
      }
    }
    if (!exists) {
      newProfile.proposals.push(informationModal);
      setCandidatesList(newProfile);
    }

    setChanged(true);
    handleClose();
  };

  const AddInformation = (type) => {
    let information = {
      id: 0,
      name: "",
      description: "",
      type: type,
    };
    setInformationModal(information);
    setOpen(true);
  };

  const candidatesContent = candidates.map((candidate) => {
    return (
      <Grid
        key={candidate.id}
        item
        container
        direction="row"
        align="center"
        className={classes.title}
      >
        <Grid item xs={2} container justify="flex-start">
          <Avatar
            alt={candidate.names}
            src={candidate.photo}
            className={classes.lista}
          />
        </Grid>
        <Grid
          item
          xs={8}
          container
          direction="column"
          justify="center"
          align="center"
        >
          <Grid item>
            <Typography
              variant="body2"
              align="left"
              style={{ color: "#3E5481", alignItems: "center" }}
            >
              {candidate.names +
                " " +
                candidate.lastNames +
                " - " +
                candidate.position}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
    );
  });

  const proposalsContent = proposals.map((proposal) => {
    return (
      <Grid key={proposal.id} item container className={classes.title}>
        <Grid item container xs={edit ? 11 : 12}>
          <Typography variant="subtitle1" className={classes.titleText}>
            {proposal.name}
          </Typography>
          <Typography
            variant="subtitle1"
            align="justify"
            className={classes.subtitleText}
          >
            {proposal.description}
          </Typography>
        </Grid>
        {edit ? (
          <Grid xs={1} item container direction="column">
            <Grid
              item
              container
              className={classes.editInfo}
              onClick={handleOpen.bind(this, proposal)}
            >
              <Create className={classes.icon} />
            </Grid>
          </Grid>
        ) : null}
      </Grid>
    );
  });

  return (
    <Grid>
      {candidatesList ? (
        <Grid
          container
          direction="column"
          align="center"
          className={classes.content}
          spacing={2}
        >
          <Grid item className={classes.backButton} onClick={Back.bind(this)}>
            <ArrowBackIos className={classes.icon} />
          </Grid>
          {candidatesList.status == "ACT" ? (
            !edit ? (
              <Grid
                item
                className={classes.editButton}
                onClick={EditButton.bind(this)}
              >
                <Create className={classes.icon} />
              </Grid>
            ) : (
              <Grid
                item
                className={classes.editButton}
                onClick={ConfirmButton.bind(this)}
              >
                <Check className={classes.icon} />
              </Grid>
            )
          ) : (
            <Grid></Grid>
          )}
          <Grid item container justify="center">
            <Avatar
              alt={candidatesList.name}
              src={candidatesList.logo}
              className={classes.avatar}
            />
          </Grid>
          <Grid item container justify="center">
            <Typography variant="h6" className={classes.titleText}>
              {candidatesList.name}
            </Typography>
          </Grid>
          {candidatesList.status !== "ACT" ? (
            <Grid item container justify="center">
              <Typography variant="h6" className={classes.titleText}>
                {candidatesList.status === "PEN"
                  ? "Su lista no fue registrada por falta de confirmación de los integrantes"
                  : "Su lista no fue aceptada por los responsables de las elecciones"}
              </Typography>
            </Grid>
          ) : (
            <Grid></Grid>
          )}
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
            spacing={1}
            className={classes.description}
          >
            <Grid item container className={classes.title}>
              <Typography variant="h6" className={classes.titleText}>
                Presentación
              </Typography>
            </Grid>
            <Grid item container className={classes.title}>
              {edit ? (
                <TextField
                  fullWidth
                  value={candidatesList.description}
                  placeholder="Presentación"
                  onChange={(event) => {
                    let presAux = { ...candidatesList };
                    presAux.description = event.target.value;
                    setChanged(true);
                    setCandidatesList(presAux);
                  }}
                  variant="outlined"
                  type="text"
                  size="small"
                  multiline
                  rows={4}
                />
              ) : (
                <Typography
                  variant="subtitle1"
                  align="justify"
                  className={classes.subtitleText}
                >
                  {candidatesList.description
                    ? candidatesList.description
                    : "No ha registrado su presentación"}
                </Typography>
              )}
            </Grid>
            <Grid item className={classes.spaceDivider} />
            <Grid item container className={classes.title}>
              <Grid xs={10} item container>
                <Typography variant="h6" className={classes.titleText}>
                  Integrantes
                </Typography>
              </Grid>
            </Grid>
            {candidatesContent[0] ? (
              candidatesContent
            ) : (
              <Grid>
                <Typography
                  variant="subtitle1"
                  className={classes.subtitleText}
                >
                  No han registrado candidatos
                </Typography>
              </Grid>
            )}
            <Grid item className={classes.spaceDivider} />
            <Grid item container className={classes.title}>
              <Grid xs={10} item container>
                <Typography variant="h6" className={classes.titleText}>
                  Propuestas
                </Typography>
              </Grid>
              {edit ? (
                <Grid
                  xs={1}
                  item
                  container
                  direction="column"
                  justify="center"
                  align="center"
                >
                  <Add
                    className={classes.add}
                    onClick={AddInformation.bind(this, "proposal")}
                  />
                </Grid>
              ) : null}
            </Grid>
            {proposalsContent[0] ? (
              proposalsContent
            ) : (
              <Grid>
                <Typography
                  variant="subtitle1"
                  className={classes.subtitleText}
                >
                  No han registrado propuestas
                </Typography>
              </Grid>
            )}
          </Grid>
        </Grid>
      ) : null}
      <Modal
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        {informationModal ? (
          <Fade in={open}>
            <Grid container className={classes.paper}>
              <Grid item container style={{ margin: "1vh" }}>
                <Grid xs={4} item container style={{ alignContent: "center" }}>
                  <Typography variant="h6" className={classes.titleText}>
                    Título
                  </Typography>
                </Grid>
                <Grid container direction="row" item xs={8} justify="center">
                  <TextField
                    fullWidth
                    value={informationModal.name}
                    placeholder="Título"
                    onChange={(event) => {
                      let infAux = { ...informationModal };
                      infAux.name = event.target.value;
                      setInformationModal(infAux);
                    }}
                    variant="outlined"
                    type="text"
                    size="small"
                  />
                </Grid>
              </Grid>
              <Grid item container style={{ margin: "1vh" }}>
                <Grid item container style={{ alignContent: "center" }}>
                  <Typography variant="h6" className={classes.titleText}>
                    Descripcion
                  </Typography>
                </Grid>
              </Grid>
              <Grid item container style={{ margin: "1vh" }}>
                <Grid container direction="row" item justify="center">
                  <TextField
                    fullWidth
                    value={informationModal.description}
                    placeholder="Descripción"
                    onChange={(event) => {
                      let infAux = { ...informationModal };
                      infAux.description = event.target.value;
                      setInformationModal(infAux);
                    }}
                    variant="outlined"
                    type="text"
                    size="small"
                    multiline
                    rows={4}
                  />
                </Grid>
              </Grid>
              <Grid
                container
                item
                xs={12}
                sm={4}
                justify="center"
                style={{ paddingBottom: "3vh", paddingTop: "3vh" }}
              >
                <Button
                  border={15}
                  variant="contained"
                  fullWidth
                  size="large"
                  color="secondary"
                  className={classes.button}
                  onClick={SaveInformation}
                >
                  Confirmar Cambios
                </Button>
              </Grid>
            </Grid>
          </Fade>
        ) : (
          <Grid></Grid>
        )}
      </Modal>
    </Grid>
  );
};

export default ListProfile;
