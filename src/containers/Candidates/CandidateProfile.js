import {
  Avatar,
  Button,
  Fade,
  Grid,
  makeStyles,
  Modal,
  TextField,
  Typography,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Backdrop from "@material-ui/core/Backdrop";
import { useHistory } from "react-router";
import { Add, ArrowBackIos, Check, Create } from "@material-ui/icons";
import informationService from "../../services/Candidates/information.service";
import userService from "../../services/Users/users.service";
import DatePicker from "../../components/DatePicker";
import * as functions from "../../functions/functions";
import * as ROUTES from "../../routes/routes";
import electionsService from "../../services/Elections/elections.service";

const useStyles = makeStyles((theme) => ({
  titleText: {
    color: "#2E3E5C",
  },
  subtitleText: {
    color: "#9FA5C0",
  },
  button: {},
  description: {
    maxWidth: "100vw",
    minHeight: "60vh",
    padding: theme.spacing(2),
  },
  content: {
    minHeight: "94vh",
    maxWidth: "100vw",
    marginLeft: 0,
    marginTop: "-5vh",
    paddingTop: "5vh",
  },
  avatar: {
    width: theme.spacing(20),
    height: theme.spacing(20),
  },
  backButton: {
    cursor: "pointer",
    top: 20,
    left: 20,
    position: "fixed",
    backgroundColor: "#000000",
    opacity: "0.2",
    borderRadius: "24px",
  },
  editButton: {
    cursor: "pointer",
    top: 20,
    right: 20,
    position: "fixed",
    backgroundColor: "#851610",
    opacity: "0.2",
    borderRadius: "24px",
  },
  editInfo: {
    cursor: "pointer",
    backgroundColor: "#851610",
    opacity: "0.2",
    borderRadius: "24px",
  },
  icon: {
    color: "#FFFFFF",
    display: "flex",
    justifyContent: "center",
  },
  add: {
    color: "#3E5481",
    cursor: "pointer",
  },
  lista: {
    width: theme.spacing(5),
    height: theme.spacing(5),
  },
  spaceDivider: {
    backgroundColor: "#D0DBEA",
    height: "2px",
    padding: "0px !important",
    width: "90vw",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    borderRadius: "10px",
    maxWidth: "90vw",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 2, 3),
  },
}));

const CandidateDetails = () => {
  const classes = useStyles();
  const history = useHistory();

  const user = JSON.parse(localStorage.getItem("user"));
  const [candidate, setCandidate] = useState();
  const [presentation, setPresentation] = useState("");
  const [professionalInformation, setProfessionalInformation] = useState([]);
  const [educationInformation, setEducationInformation] = useState([]);
  const [changed, setChanged] = useState(false);
  const [solicited, setSolicited] = useState(false);

  const [open, setOpen] = React.useState(false);
  const [edit, setEdit] = useState(false);
  const [informationModal, setInformationModal] = useState();
  const [dateStart, setDateStart] = React.useState(new Date());
  const [dateEnd, setDateEnd] = React.useState(new Date());

  const GetInformation = (candidato) => {
    informationService
      .getCandidateInformation(
        candidato.id,
        candidato.candidatesLists[0].id,
        "profile"
      )
      .then((response) => {
        if (response.data) {
          if (response.data[0].status === "SOL") setSolicited(true);
          setProfessionalInformation(
            response.data.filter(
              (information) => information.type === "professional experience"
            )
          );
          setEducationInformation(
            response.data.filter(
              (information) => information.type === "education experience"
            )
          );

          setPresentation(
            response.data.find(
              (information) => information.type === "description"
            )
          );
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetUser = () => {
    userService
      .getUser(user.id)
      .then((response) => {
        if (response.data) {
          setCandidate(response.data);
          GetInformation(response.data);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    electionsService
      .getStage(user.id)
      .then((response) => {
        if (response.data) {
          if (response.data !== "actualization") history.push(ROUTES.PERFIL);
          else GetUser();
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }, [dateStart, dateEnd]);

  const Back = () => {
    history.goBack();
  };

  const EditButton = () => {
    setEdit(true);
  };

  const ConfirmButton = () => {
    if (changed) {
      let newProfileInf = [];
      newProfileInf = [...professionalInformation].concat([
        ...educationInformation,
      ]);
      newProfileInf.push(presentation);
      informationService.saveCandidateInformation(
        candidate.id,
        candidate.candidatesLists[0].id,
        "SOL",
        newProfileInf
      );
    }
    setEdit(false);
  };

  const handleOpen = (information) => {
    setInformationModal(information);
    setOpen(true);
  };

  const handleClose = () => {
    console.log(informationModal);
    setOpen(false);
  };

  const SaveInformation = () => {
    let infAux = [];
    if (informationModal.type === "professional experience")
      infAux = [...professionalInformation];
    else infAux = [...educationInformation];
    let exists = false;
    for (let index = 0; index < infAux.length; index++) {
      if (infAux[index].id === informationModal.id) {
        exists = true;
        infAux[index] = informationModal;
        infAux[index].startDate = functions.convertToFormatBackend(dateStart);
        infAux[index].endDate = functions.convertToFormatBackend(dateEnd);
        if (informationModal.type === "professional experience")
          setProfessionalInformation(infAux);
        else setEducationInformation(infAux);
      }
    }
    if (!exists) {
      infAux.push(informationModal);
      if (informationModal.type === "professional experience")
        setProfessionalInformation(infAux);
      else setEducationInformation(infAux);
    }
    setChanged(true);
    handleClose();
  };

  const AddInformation = (type) => {
    let information = {
      id: 0,
      role: "",
      enterprise: "",
      description: "",
      startDate: null,
      endDate: null,
      type: type,
    };
    setInformationModal(information);
    setOpen(true);
  };

  const professionalExperienceContent = professionalInformation.map(
    (information) => {
      return (
        <Grid key={information.id} item container className={classes.title}>
          <Grid item container xs={edit ? 11 : 12}>
            <Typography
              variant="subtitle1"
              className={classes.subtitleText}
              align="justify"
            >
              {information.role + " - " + information.enterprise}
            </Typography>
            <Typography
              variant="subtitle1"
              className={classes.subtitleText}
              align="justify"
            >
              {information.endDate
                ? information.startDate.split("-").reverse().join("/") +
                  " - " +
                  information.endDate.split("-").reverse().join("/")
                : information.startDate.split("-").reverse().join("/") +
                  " - Actualidad"}
            </Typography>
          </Grid>
          {edit ? (
            <Grid xs={1} item container direction="column">
              <Grid
                item
                container
                className={classes.editInfo}
                onClick={handleOpen.bind(this, information)}
              >
                <Create className={classes.icon} />
              </Grid>
            </Grid>
          ) : null}
        </Grid>
      );
    }
  );

  const educationExperienceContent = educationInformation.map((information) => {
    return (
      <Grid key={information.id} item container className={classes.title}>
        <Grid item container xs={edit ? 11 : 12}>
          <Typography
            variant="subtitle1"
            className={classes.subtitleText}
            align="justify"
          >
            {information.role + " - " + information.enterprise}
          </Typography>
          <Typography
            variant="subtitle1"
            className={classes.subtitleText}
            align="justify"
          >
            {information.endDate
              ? information.startDate.split("-").reverse().join("/") +
                " - " +
                information.endDate.split("-").reverse().join("/")
              : information.startDate.split("-").reverse().join("/") +
                " - Actualidad"}
          </Typography>
        </Grid>
        {edit ? (
          <Grid xs={1} item container direction="column">
            <Grid
              item
              container
              className={classes.editInfo}
              onClick={handleOpen.bind(this, information)}
            >
              <Create className={classes.icon} />
            </Grid>
          </Grid>
        ) : null}
      </Grid>
    );
  });

  const changeStartDate = (date) => {
    setDateStart(date);
    let infAux = { ...informationModal };
    infAux.startDate = functions.convertToFormatBackend(date);
    setInformationModal(infAux);
  };

  const changeEndDate = (date) => {
    setDateEnd(date);
    let infAux = { ...informationModal };
    infAux.endDate = functions.convertToFormatBackend(date);
    setInformationModal(infAux);
  };

  return (
    <Grid>
      {candidate ? (
        <Grid
          container
          direction="column"
          align="center"
          className={classes.content}
          spacing={2}
        >
          <Grid item className={classes.backButton} onClick={Back.bind(this)}>
            <ArrowBackIos className={classes.icon} />
          </Grid>
          {!solicited ? (
            !edit ? (
              <Grid
                item
                className={classes.editButton}
                onClick={EditButton.bind(this)}
              >
                <Create className={classes.icon} />
              </Grid>
            ) : (
              <Grid
                item
                className={classes.editButton}
                onClick={ConfirmButton.bind(this)}
              >
                <Check className={classes.icon} />
              </Grid>
            )
          ) : null}
          <Grid item container justify="center">
            <Avatar
              alt={candidate.names}
              src={candidate.photo}
              className={classes.avatar}
            />
          </Grid>
          <Grid item container justify="center">
            <Typography variant="h6" className={classes.titleText}>
              {candidate.names + " " + candidate.lastNames}
            </Typography>
          </Grid>
          {solicited ? (
            <Grid item container justify="center">
              <Typography
                variant="h6"
                className={classes.titleText}
                style={{ color: "red" }}
              >
                {"Actualización de Información Solicitada"}
              </Typography>
            </Grid>
          ) : null}
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
            spacing={1}
            className={classes.description}
          >
            <Grid item container className={classes.title}>
              <Typography variant="subtitle1" className={classes.subtitleText}>
                {candidate.position}
              </Typography>
            </Grid>
            <Grid
              item
              container
              direction="row"
              align="center"
              className={classes.title}
            >
              <Grid item xs={2} container justify="flex-start">
                <Avatar
                  alt={candidate.candidatesLists[0].name}
                  src={candidate.candidatesLists[0].logo}
                  className={classes.lista}
                />
              </Grid>
              <Grid
                item
                xs={8}
                container
                direction="column"
                justify="center"
                align="center"
              >
                <Grid item>
                  <Typography
                    variant="body2"
                    align="left"
                    style={{ color: "#3E5481", alignItems: "center" }}
                  >
                    {candidate.candidatesLists[0].name}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item className={classes.spaceDivider} />
            <Grid item container className={classes.title}>
              <Typography variant="h6" className={classes.titleText}>
                Presentación
              </Typography>
            </Grid>
            <Grid item container className={classes.title}>
              {edit ? (
                <TextField
                  fullWidth
                  value={
                    presentation
                      ? presentation.description
                      : "No ha registrado su presentación"
                  }
                  placeholder="Presentación"
                  onChange={(event) => {
                    let presAux = { ...presentation };
                    presAux.description = event.target.value;
                    setChanged(true);
                    setPresentation(presAux);
                  }}
                  variant="outlined"
                  type="text"
                  size="small"
                  multiline
                  rows={4}
                />
              ) : (
                <Typography
                  variant="subtitle1"
                  align="justify"
                  className={classes.subtitleText}
                >
                  {presentation
                    ? presentation.description
                    : "No ha registrado su presentación"}
                </Typography>
              )}
            </Grid>
            <Grid item className={classes.spaceDivider} />
            <Grid item container direction="row" className={classes.title}>
              <Grid xs={10} item container>
                <Typography variant="h6" className={classes.titleText}>
                  Experiencia Profesional
                </Typography>
              </Grid>
              {edit ? (
                <Grid
                  xs={1}
                  item
                  container
                  direction="column"
                  justify="center"
                  align="center"
                >
                  <Add
                    className={classes.add}
                    onClick={AddInformation.bind(
                      this,
                      "professional experience"
                    )}
                  />
                </Grid>
              ) : null}
            </Grid>
            {professionalExperienceContent[0] ? (
              professionalExperienceContent
            ) : (
              <Grid>
                <Typography
                  variant="subtitle1"
                  className={classes.subtitleText}
                >
                  No ha registrado experiencias profesionales
                </Typography>
              </Grid>
            )}
            <Grid item className={classes.spaceDivider} />
            <Grid item container className={classes.title}>
              <Grid xs={10} item container>
                <Typography variant="h6" className={classes.titleText}>
                  Educación
                </Typography>
              </Grid>
              {edit ? (
                <Grid
                  xs={1}
                  item
                  container
                  direction="column"
                  justify="center"
                  align="center"
                >
                  <Add
                    className={classes.add}
                    onClick={AddInformation.bind(this, "education experience")}
                  />
                </Grid>
              ) : null}
            </Grid>

            {educationExperienceContent[0] ? (
              educationExperienceContent
            ) : (
              <Grid>
                <Typography
                  variant="subtitle1"
                  className={classes.subtitleText}
                >
                  No ha registrado su información académica
                </Typography>
              </Grid>
            )}
          </Grid>
        </Grid>
      ) : null}
      <Modal
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        {informationModal ? (
          informationModal.type === "professional experience" ? (
            <Fade in={open}>
              <Grid container className={classes.paper}>
                <Grid item container style={{ margin: "1vh" }}>
                  <Grid
                    xs={4}
                    item
                    container
                    style={{ alignContent: "center" }}
                  >
                    <Typography variant="h6" className={classes.titleText}>
                      Cargo
                    </Typography>
                  </Grid>
                  <Grid container direction="row" item xs={8} justify="center">
                    <TextField
                      fullWidth
                      value={informationModal.role}
                      placeholder="Cargo"
                      onChange={(event) => {
                        let infAux = { ...informationModal };
                        infAux.role = event.target.value;
                        setInformationModal(infAux);
                      }}
                      variant="outlined"
                      type="text"
                      size="small"
                    />
                  </Grid>
                </Grid>
                <Grid item container style={{ margin: "1vh" }}>
                  <Grid
                    xs={4}
                    item
                    container
                    style={{ alignContent: "center" }}
                  >
                    <Typography variant="h6" className={classes.titleText}>
                      Empresa
                    </Typography>
                  </Grid>
                  <Grid container direction="row" item xs={8} justify="center">
                    <TextField
                      fullWidth
                      value={informationModal.enterprise}
                      placeholder="Empresa"
                      onChange={(event) => {
                        let infAux = { ...informationModal };
                        infAux.enterprise = event.target.value;
                        setInformationModal(infAux);
                      }}
                      variant="outlined"
                      type="text"
                      size="small"
                    />
                  </Grid>
                </Grid>
                <Grid item container style={{ margin: "1vh" }}>
                  <Grid
                    xs={4}
                    item
                    container
                    style={{ alignContent: "center" }}
                  >
                    <Typography variant="h6" className={classes.titleText}>
                      Inicio
                    </Typography>
                  </Grid>
                  <Grid container direction="row" item xs={8} justify="center">
                    <DatePicker
                      label={"Fecha de inicio"}
                      selectedDate={dateStart}
                      handleDateChange={changeStartDate}
                    />
                  </Grid>
                </Grid>
                <Grid item container style={{ margin: "1vh" }}>
                  <Grid
                    xs={4}
                    item
                    container
                    style={{ alignContent: "center" }}
                  >
                    <Typography variant="h6" className={classes.titleText}>
                      Fin
                    </Typography>
                  </Grid>
                  <Grid container direction="row" item xs={8} justify="center">
                    <DatePicker
                      label={"Fecha de Fin"}
                      selectedDate={dateEnd}
                      handleDateChange={changeEndDate}
                    />
                  </Grid>
                </Grid>
                <Grid item container style={{ margin: "1vh" }}>
                  <Grid item container style={{ alignContent: "center" }}>
                    <Typography variant="h6" className={classes.titleText}>
                      Descripcion
                    </Typography>
                  </Grid>
                </Grid>
                <Grid item container style={{ margin: "1vh" }}>
                  <Grid container direction="row" item justify="center">
                    <TextField
                      fullWidth
                      value={informationModal.description}
                      placeholder="Descripción"
                      onChange={(event) => {
                        let infAux = { ...informationModal };
                        infAux.description = event.target.value;
                        setInformationModal(infAux);
                      }}
                      variant="outlined"
                      type="text"
                      size="small"
                      multiline
                      rows={4}
                    />
                  </Grid>
                </Grid>
                <Grid
                  container
                  item
                  xs={12}
                  sm={4}
                  justify="center"
                  style={{ paddingBottom: "3vh", paddingTop: "3vh" }}
                >
                  <Button
                    border={15}
                    variant="contained"
                    fullWidth
                    size="large"
                    color="secondary"
                    className={classes.button}
                    onClick={SaveInformation}
                  >
                    Confirmar Cambios
                  </Button>
                </Grid>
              </Grid>
            </Fade>
          ) : (
            <Fade in={open}>
              <Grid container className={classes.paper}>
                <Grid item container style={{ margin: "1vh" }}>
                  <Grid
                    xs={4}
                    item
                    container
                    style={{ alignContent: "center" }}
                  >
                    <Typography variant="h6" className={classes.titleText}>
                      Título
                    </Typography>
                  </Grid>
                  <Grid container direction="row" item xs={8} justify="center">
                    <TextField
                      fullWidth
                      value={informationModal.role}
                      placeholder="Cargo"
                      onChange={(event) => {
                        let infAux = { ...informationModal };
                        infAux.role = event.target.value;
                        setInformationModal(infAux);
                      }}
                      variant="outlined"
                      type="text"
                      size="small"
                    />
                  </Grid>
                </Grid>
                <Grid item container style={{ margin: "1vh" }}>
                  <Grid
                    xs={4}
                    item
                    container
                    style={{ alignContent: "center" }}
                  >
                    <Typography variant="h6" className={classes.titleText}>
                      Centro Educativo
                    </Typography>
                  </Grid>
                  <Grid
                    container
                    direction="column"
                    item
                    xs={8}
                    justify="center"
                    align="center"
                  >
                    <TextField
                      fullWidth
                      value={informationModal.enterprise}
                      placeholder="Empresa"
                      onChange={(event) => {
                        let infAux = { ...informationModal };
                        infAux.enterprise = event.target.value;
                        setInformationModal(infAux);
                      }}
                      variant="outlined"
                      type="text"
                      size="small"
                    />
                  </Grid>
                </Grid>
                <Grid item container style={{ margin: "1vh" }}>
                  <Grid
                    xs={4}
                    item
                    container
                    style={{ alignContent: "center" }}
                  >
                    <Typography variant="h6" className={classes.titleText}>
                      Inicio
                    </Typography>
                  </Grid>
                  <Grid container direction="row" item xs={8} justify="center">
                    <DatePicker
                      label={"Fecha de inicio"}
                      selectedDate={dateStart}
                      handleDateChange={changeStartDate}
                    />
                  </Grid>
                </Grid>
                <Grid item container style={{ margin: "1vh" }}>
                  <Grid
                    xs={4}
                    item
                    container
                    style={{ alignContent: "center" }}
                  >
                    <Typography variant="h6" className={classes.titleText}>
                      Fin
                    </Typography>
                  </Grid>
                  <Grid container direction="row" item xs={8} justify="center">
                    <DatePicker
                      label={"Fecha de Fin"}
                      selectedDate={dateEnd}
                      handleDateChange={changeEndDate}
                    />
                  </Grid>
                </Grid>
                <Grid
                  container
                  item
                  xs={12}
                  sm={4}
                  justify="center"
                  style={{ paddingBottom: "3vh", paddingTop: "3vh" }}
                >
                  <Button
                    border={15}
                    variant="contained"
                    fullWidth
                    size="large"
                    color="secondary"
                    className={classes.button}
                    onClick={SaveInformation}
                  >
                    Confirmar Cambios
                  </Button>
                </Grid>
              </Grid>
            </Fade>
          )
        ) : (
          <Grid></Grid>
        )}
      </Modal>
    </Grid>
  );
};

export default CandidateDetails;
