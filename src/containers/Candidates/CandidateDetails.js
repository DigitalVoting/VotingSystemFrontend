import { Avatar, Grid, makeStyles, Paper, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import * as ROUTES from "../../routes/routes";
import { ArrowBackIos } from "@material-ui/icons";
import informationService from "../../services/Candidates/information.service";

const useStyles = makeStyles((theme) => ({
  title: {
    maxWidth: "90vw",
  },
  titleText: {
    color: "#2E3E5C",
  },
  subtitleText: {
    color: "#9FA5C0",
  },
  button: {},
  description: {
    maxWidth: "100vw",
    minHeight: "60vh",
  },
  content: {
    minHeight: "94vh",
    maxWidth: "100vw",
    background: "#D1B059",
    marginLeft: 0,
    marginTop: "-5vh",
    paddingTop: "5vh",
  },
  avatar: {
    width: theme.spacing(20),
    height: theme.spacing(20),
  },
  backButton: {
    cursor: "pointer",
    top: 20,
    left: 20,
    position: "fixed",
    backgroundColor: "#000000",
    opacity: "0.2",
    borderRadius: "24px",
  },
  icon: {
    color: "#FFFFFF",
    display: "flex",
    justifyContent: "center",
  },
  paperBottom: {
    paddingTop: theme.spacing(6),
    paddingBottom: theme.spacing(4),
    background: "white",
    borderTopRightRadius: "10%",
    borderTopLeftRadius: "10%",
    margin: "0",
  },
  lista: {
    width: theme.spacing(5),
    height: theme.spacing(5),
  },
  spaceDivider: {
    backgroundColor: "#D0DBEA",
    height: "2px",
    padding: "0px !important",
    width: "90vw",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}));

const CandidateDetails = () => {
  const classes = useStyles();
  const history = useHistory();

  const candidate = JSON.parse(localStorage.getItem("details"));
  const [presentation, setPresentation] = useState("");
  const [professionalInformation, setProfessionalInformation] = useState([]);
  const [educationInformation, setEducationInformation] = useState([]);

  useEffect(() => {
    informationService
      .getCandidateInformation(
        candidate.id,
        candidate.candidatesLists[0].id,
        "actual"
      )
      .then((response) => {
        if (response.data) {
          setProfessionalInformation(
            response.data.filter(
              (information) => information.type === "professional experience"
            )
          );
          setEducationInformation(
            response.data.filter(
              (information) => information.type === "education experience"
            )
          );

          setPresentation(
            response.data.find(
              (information) => information.type === "description"
            )
          );
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  const Back = () => {
    history.push(ROUTES.CANDIDATOS);
  };

  const professionalExperienceContent = professionalInformation.map(
    (information) => {
      return (
        <Grid key={information.id} item container className={classes.title}>
          <Typography
            variant="subtitle1"
            className={classes.subtitleText}
            align="justify"
          >
            {information.role + " - " + information.enterprise}
          </Typography>
          <Typography
            variant="subtitle1"
            className={classes.subtitleText}
            align="justify"
          >
            {information.endDate
              ? information.startDate.split("-").reverse().join("/") +
                " - " +
                information.endDate.split("-").reverse().join("/")
              : information.startDate.split("-").reverse().join("/") +
                " - Actualidad"}
          </Typography>
        </Grid>
      );
    }
  );

  const educationExperienceContent = educationInformation.map((information) => {
    return (
      <Grid key={information.id} item container className={classes.title}>
        <Typography
          variant="subtitle1"
          className={classes.subtitleText}
          align="justify"
        >
          {information.role + " - " + information.enterprise}
        </Typography>
        <Typography
          variant="subtitle1"
          className={classes.subtitleText}
          align="justify"
        >
          {information.endDate
            ? information.startDate.split("-").reverse().join("/") +
              " - " +
              information.endDate.split("-").reverse().join("/")
            : information.startDate.split("-").reverse().join("/") +
              " - Actualidad"}
        </Typography>
      </Grid>
    );
  });

  return (
    <Grid
      container
      direction="column"
      align="center"
      className={classes.content}
      spacing={2}
    >
      <Grid item className={classes.backButton} onClick={Back.bind(this)}>
        <ArrowBackIos className={classes.icon} />
      </Grid>
      <Grid item container justify="center">
        <Avatar
          alt={candidate.names}
          src={candidate.photo}
          className={classes.avatar}
        />
      </Grid>
      <Paper className={classes.paperBottom}>
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
          spacing={1}
          className={classes.description}
        >
          <Grid item container className={classes.title}>
            <Typography variant="h6" className={classes.titleText}>
              {candidate.names + " " + candidate.lastNames}
            </Typography>
          </Grid>
          <Grid item container className={classes.title}>
            <Typography variant="subtitle1" className={classes.subtitleText}>
              {candidate.position}
            </Typography>
          </Grid>
          <Grid
            item
            container
            direction="row"
            align="center"
            className={classes.title}
          >
            <Grid item xs={2} container justify="flex-start">
              <Avatar
                alt={candidate.candidatesLists[0].name}
                src={candidate.candidatesLists[0].logo}
                className={classes.lista}
              />
            </Grid>
            <Grid
              item
              xs={8}
              container
              direction="column"
              justify="center"
              align="center"
            >
              <Grid item>
                <Typography
                  variant="body2"
                  align="left"
                  style={{ color: "#3E5481", alignItems: "center" }}
                >
                  {candidate.candidatesLists[0].name}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid item className={classes.spaceDivider} />
          <Grid item container className={classes.title}>
            <Typography variant="h6" className={classes.titleText}>
              Presentación
            </Typography>
          </Grid>
          <Grid item container className={classes.title}>
            <Typography
              variant="subtitle1"
              align="justify"
              className={classes.subtitleText}
            >
              {presentation
                ? presentation.description
                : "No ha registrado su presentación"}
            </Typography>
          </Grid>
          <Grid item className={classes.spaceDivider} />
          <Grid item container className={classes.title}>
            <Typography variant="h6" className={classes.titleText}>
              Experiencia Profesional
            </Typography>
          </Grid>
          {professionalExperienceContent[0] ? (
            professionalExperienceContent
          ) : (
            <Grid>
              <Typography variant="subtitle1" className={classes.subtitleText}>
                No ha registrado experiencias profesionales
              </Typography>
            </Grid>
          )}
          <Grid item className={classes.spaceDivider} />
          <Grid item container className={classes.title}>
            <Typography variant="h6" className={classes.titleText}>
              Educación
            </Typography>
          </Grid>

          {educationExperienceContent[0] ? (
            educationExperienceContent
          ) : (
            <Grid>
              <Typography variant="subtitle1" className={classes.subtitleText}>
                No ha registrado su información académica
              </Typography>
            </Grid>
          )}
        </Grid>
      </Paper>
    </Grid>
  );
};

export default CandidateDetails;
