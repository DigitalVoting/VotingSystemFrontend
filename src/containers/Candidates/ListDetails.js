import { Avatar, Grid, makeStyles, Paper, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router";
import * as ROUTES from "../../routes/routes";
import { ArrowBackIos, ArrowForwardIos } from "@material-ui/icons";
import candidatesListService from "../../services/Candidates/candidate-list.service";

const useStyles = makeStyles((theme) => ({
  title: {
    maxWidth: "90vw",
  },
  titleText: {
    color: "#2E3E5C",
  },
  subtitleText: {
    color: "#9FA5C0",
  },
  button: {},
  description: {
    maxWidth: "100vw",
    minHeight: "60vh",
  },
  content: {
    minHeight: "94vh",
    maxWidth: "100vw",
    background: "#D1B059",
    marginLeft: 0,
    marginTop: "-5vh",
    paddingTop: "5vh",
  },
  avatar: {
    width: theme.spacing(20),
    height: theme.spacing(20),
    background: "white",
  },
  backButton: {
    cursor: "pointer",
    top: 20,
    left: 20,
    position: "fixed",
    backgroundColor: "#000000",
    opacity: "0.2",
    borderRadius: "24px",
  },
  icon: {
    color: "#FFFFFF",
    display: "flex",
    justifyContent: "center",
  },
  paperBottom: {
    paddingTop: theme.spacing(6),
    paddingBottom: theme.spacing(4),
    background: "white",
    borderTopRightRadius: "10%",
    borderTopLeftRadius: "10%",
    margin: "0",
  },
  lista: {
    width: theme.spacing(5),
    height: theme.spacing(5),
    background: "white",
    color: "gray",
  },
  spaceDivider: {
    backgroundColor: "#D0DBEA",
    height: "2px",
    padding: "0px !important",
    width: "90vw",
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
}));

const ListDetails = () => {
  const classes = useStyles();
  const history = useHistory();

  const candidatesList = JSON.parse(localStorage.getItem("details"));
  const [candidates, setCandidates] = useState([]);

  useEffect(() => {
    candidatesListService
      .getCandidatesOfCandidatesList(candidatesList.id)
      .then((response) => {
        if (response.data) {
          setCandidates(response.data);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  const Back = () => {
    history.push(ROUTES.CANDIDATOS);
  };

  const CandidateDetailsPage = (candidate) => {
    localStorage.setItem("details", JSON.stringify(candidate));
    history.push(ROUTES.CANDIDATO_DETALLES);
  };

  const candidatesContent = candidates.map((candidate) => {
    return (
      <Grid
        key={candidate.id}
        item
        container
        direction="row"
        align="center"
        className={classes.title}
      >
        <Grid item xs={2} container justify="flex-start">
          <Avatar
            alt={candidate.names}
            src={candidate.photo}
            className={classes.lista}
          />
        </Grid>
        <Grid
          item
          xs={8}
          container
          direction="column"
          justify="center"
          align="center"
        >
          <Grid item>
            <Typography
              variant="body2"
              align="left"
              style={{ color: "#3E5481", alignItems: "center" }}
            >
              {candidate.names +
                " " +
                candidate.lastNames +
                " - " +
                candidate.position}
            </Typography>
          </Grid>
        </Grid>
        <Grid
          item
          xs={2}
          container
          justify="center"
          alignContent="center"
          style={{ cursor: "pointer" }}
          onClick={CandidateDetailsPage.bind(this, candidate)}
        >
          <ArrowForwardIos />
        </Grid>
      </Grid>
    );
  });

  const proposalsContent = candidatesList.proposals.map((proposal) => {
    return (
      <Grid key={proposal.id} item container className={classes.title}>
        <Typography variant="subtitle1" className={classes.titleText}>
          {proposal.name}
        </Typography>
        <Typography
          variant="subtitle1"
          align="justify"
          className={classes.subtitleText}
        >
          {proposal.description}
        </Typography>
      </Grid>
    );
  });

  return (
    <Grid
      container
      direction="column"
      align="center"
      className={classes.content}
      spacing={2}
    >
      <Grid item className={classes.backButton} onClick={Back.bind(this)}>
        <ArrowBackIos className={classes.icon} />
      </Grid>
      <Grid item container justify="center">
        <Avatar
          alt={candidatesList.name}
          src={candidatesList.logo}
          className={classes.avatar}
        />
      </Grid>
      <Paper className={classes.paperBottom}>
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
          spacing={1}
          className={classes.description}
        >
          <Grid item container className={classes.title}>
            <Typography variant="h6" className={classes.titleText}>
              {candidatesList.name}
            </Typography>
          </Grid>
          <Grid item className={classes.spaceDivider} />
          <Grid item container className={classes.title}>
            <Typography variant="h6" className={classes.titleText}>
              Presentación
            </Typography>
          </Grid>
          <Grid item container className={classes.title}>
            <Typography
              variant="subtitle1"
              align="justify"
              className={classes.subtitleText}
            >
              {candidatesList
                ? candidatesList.description
                : "No ha registrado su presentación"}
            </Typography>
          </Grid>
          <Grid item className={classes.spaceDivider} />
          <Grid item container className={classes.title}>
            <Typography variant="h6" className={classes.titleText}>
              Integrantes
            </Typography>
          </Grid>
          {candidatesContent[0] ? (
            candidatesContent
          ) : (
            <Grid>
              <Typography variant="subtitle1" className={classes.subtitleText}>
                No han registrado candidatos
              </Typography>
            </Grid>
          )}
          <Grid item className={classes.spaceDivider} />
          <Grid item container className={classes.title}>
            <Typography variant="h6" className={classes.titleText}>
              Propuestas
            </Typography>
          </Grid>
          {proposalsContent[0] ? (
            proposalsContent
          ) : (
            <Grid>
              <Typography variant="subtitle1" className={classes.subtitleText}>
                No han registrado propuestas
              </Typography>
            </Grid>
          )}
        </Grid>
      </Paper>
    </Grid>
  );
};

export default ListDetails;
