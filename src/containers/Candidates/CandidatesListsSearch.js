import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ListCard from "../../components/ListCard";
import {
  Box,
  InputAdornment,
  Tab,
  Tabs,
  TextField,
  Typography,
} from "@material-ui/core";
import { Search } from "@material-ui/icons";
import PropTypes from "prop-types";
import CandidateCard from "../../components/CandidateCard";
import candidatesListService from "../../services/Candidates/candidate-list.service";
import candidatesService from "../../services/Candidates/candidate.service";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: "100vw",
  },
  titleSection: {
    marginTop: theme.spacing(2),
  },
  searchContent: {
    borderRadius: 24,
  },
  searchField: {
    borderRadius: 24,
    background: "#F4F5F7",
  },
  icon: {
    color: "#3E5481",
    cursor: "pointer",
  },
  spaceDivider: {
    backgroundColor: "#F4F5F7",
    minHeight: "1vh",
    width: "100vw",
    marginTop: theme.spacing(2),
  },
  tabs: {
    width: "100vw",
  },
  tab: {
    width: "50vw",
  },
}));

const CandidatesListSearch = () => {
  const classes = useStyles();

  const [searchText, setSearchText] = useState("");
  const [candidatesLists, setCandidatesLists] = useState([]);
  const [candidates, setCandidates] = useState([]);
  const [value, setValue] = React.useState(0);

  const [online, setOnline] = useState(navigator.onLine);

  window.addEventListener("offline", function () {
    if (online) {
      localStorage.setItem("candidates", JSON.stringify(candidates));
      localStorage.setItem("candidatesLists", JSON.stringify(candidatesLists));
      setOnline(false);
    }
  });

  window.addEventListener("online", function () {
    if (!online) {
      localStorage.removeItem("candidates");
      localStorage.removeItem("candidatesLists");
      GetCandidatesListsCurrent();
      GetCandidatesCurrent();
      setOnline(true);
    }
  });

  const handleChange = (event, newValue) => {
    setValue(newValue);
    setSearchText("");
    if (newValue) GetCandidatesCurrent();
    else GetCandidatesListsCurrent();
  };

  const GetCandidatesListsCurrent = () => {
    candidatesListService
      .getCandidatesListsCurrent()
      .then((response) => {
        if (response.data) {
          setCandidatesLists(response.data);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetCandidatesCurrent = () => {
    candidatesService
      .getCandidatesCurrent()
      .then((response) => {
        if (response.data) {
          setCandidates(response.data);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };
  useEffect(() => {
    if (navigator.onLine) {
      GetCandidatesListsCurrent();
      GetCandidatesCurrent();
      const candidatesSaved = JSON.parse(localStorage.getItem("candidates"));
      const candidatesListsSaved = JSON.parse(
        localStorage.getItem("candidatesLists")
      );
      if (candidatesSaved) {
        localStorage.removeItem("candidates");
      }
      if (candidatesListsSaved) {
        localStorage.removeItem("candidatesLists");
      }
    } else {
      const candidatesSaved = JSON.parse(localStorage.getItem("candidates"));
      const candidatesListsSaved = JSON.parse(
        localStorage.getItem("candidatesLists")
      );
      if (candidatesSaved) {
        setCandidates(candidatesSaved);
      }
      if (candidatesListsSaved) {
        setCandidatesLists(candidatesListsSaved);
      }
    }
  }, []);

  const onKeyUp = (event) => {
    if (event.charCode === 13) {
      SearchText();
    }
  };

  const SearchText = () => {
    if (value) {
      candidatesService
        .findCandidatesCurrent(searchText)
        .then((response) => {
          if (response.data) {
            setCandidates(response.data);
          }
        })
        .catch((e) => {
          console.log(e);
        });
    } else {
      candidatesListService
        .findCandidatesListsCurrent(searchText)
        .then((response) => {
          if (response.data) {
            setCandidatesLists(response.data);
          }
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };

  return (
    <Grid>
      <Grid container direction="column" alignContent="center" justify="center">
        <Grid item container justify="center" className={classes.content}>
          <Grid
            item
            container
            xs={10}
            justify="center"
            className={classes.searchContent}
          >
            <TextField
              className={classes.searchField}
              fullWidth
              value={searchText}
              placeholder="Buscar"
              onChange={(event) => {
                setSearchText(event.target.value);
              }}
              variant="outlined"
              type="text"
              size="small"
              autoComplete="current-user"
              inputProps={{ maxLength: 20 }}
              InputProps={{
                endAdornment: (
                  <InputAdornment
                    position="end"
                    className={classes.icon}
                    onClick={SearchText}
                  >
                    <Search />
                  </InputAdornment>
                ),
              }}
              onKeyPress={onKeyUp}
            />
          </Grid>
        </Grid>

        <Grid item className={classes.spaceDivider} />

        <Grid item container justify="center" className={classes.content}>
          <Tabs centered value={value} onChange={handleChange}>
            <Tab label="Listas" {...a11yProps(0)} className={classes.tab} />
            <Tab label="Candidatos" {...a11yProps(1)} className={classes.tab} />
          </Tabs>
        </Grid>

        <TabPanel value={value} index={0}>
          <Grid item container justify="flex-start" className={classes.content}>
            <Grid
              item
              container
              justify="flex-start"
              align="flex-start"
              className={classes.root}
              spacing={3}
            >
              {candidatesLists.map((list) => {
                return (
                  <Grid key={list.id} item xs={6} md={3}>
                    <ListCard
                      keyID={list.id}
                      titleList={list.name}
                      imageList={list.logo}
                      imageTitle={list.name}
                      list={list}
                    />
                  </Grid>
                );
              })}
              {candidatesLists[0] ? (
                <Grid></Grid>
              ) : (
                <Grid item>
                  <Typography variant="h6" className={classes.name}>
                    {"No hay registros disponibles"}
                  </Typography>
                </Grid>
              )}
            </Grid>
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Grid item container justify="flex-start" className={classes.content}>
            <Grid
              item
              container
              justify="flex-start"
              align="flex-start"
              className={classes.root}
              spacing={3}
            >
              {candidates.map((candidate) => {
                return (
                  <Grid key={candidate.id} item xs={6} md={3}>
                    <CandidateCard
                      keyID={candidate.id}
                      name={candidate.names + " " + candidate.lastNames}
                      list={candidate.candidatesLists[0].name}
                      role={candidate.position}
                      image={candidate.photo}
                      description={candidate.names}
                      listLogo={candidate.candidatesLists[0].logo}
                      listLogoDescription={candidate.candidatesLists[0].name}
                      candidate={candidate}
                    />
                  </Grid>
                );
              })}
              {candidates[0] ? (
                <Grid></Grid>
              ) : (
                <Grid item>
                  <Typography variant="h6" className={classes.name}>
                    {"No hay registros disponibles"}
                  </Typography>
                </Grid>
              )}
            </Grid>
          </Grid>
        </TabPanel>
      </Grid>
    </Grid>
  );
};
export default CandidatesListSearch;
