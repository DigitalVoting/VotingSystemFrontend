import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Banner from "../components/Banner";
import bannerSevice from "../services/Elections/banners.service";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: "100vw",
  },
  titleSection: {
    marginTop: theme.spacing(2),
  },
}));

const Home = () => {
  const classes = useStyles();
  const [banners, setBanners] = useState([]);
  const [online, setOnline] = useState(navigator.onLine);

  window.addEventListener("offline", function () {
    if (online) {
      setOnline(false);
      localStorage.setItem("banners", JSON.stringify(banners));
    }
  });

  window.addEventListener("online", function () {
    if (!online) {
      localStorage.removeItem("banners");
      setOnline(true);
      GetBanners();
    }
  });

  const GetBanners = () => {
    bannerSevice
      .getBanners()
      .then((response) => {
        if (response.data) {
          setBanners(response.data);
        } else {
          console.log("No data");
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    if (navigator.onLine) {
      GetBanners();
      const bannersSaved = JSON.parse(localStorage.getItem("banners"));
      if (bannersSaved) {
        localStorage.removeItem("banners");
      }
    } else {
      const bannersSaved = JSON.parse(localStorage.getItem("banners"));
      if (bannersSaved) {
        setBanners(bannersSaved);
      }
    }
  }, []);

  return (
    <Grid container justify="center">
      <Grid
        container
        direction="column"
        justify="center"
        align="center"
        className={classes.root}
        spacing={3}
      >
        {banners.map((banner) => {
          return (
            <Grid key={banner.id} item>
              <Banner
                keyID={banner.id}
                titleBanner={banner.title}
                imageBanner={banner.photo}
                imageTitle={banner.title}
              />
            </Grid>
          );
        })}
        {banners[0] ? (
          <Grid></Grid>
        ) : (
          <Grid item>
            <Banner
              keyID={0}
              titleBanner={"No hay información disponible"}
              imageBanner={null}
              imageTitle={""}
            />
          </Grid>
        )}
      </Grid>
    </Grid>
  );
};
export default Home;
