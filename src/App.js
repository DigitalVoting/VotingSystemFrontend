import React from "react";
import "./App.css";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import { ThemeProvider } from "@material-ui/styles";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import * as ROUTES from "./routes/routes";
import Login from "./containers/Session/Login";
import HomeComponent from "./containers/Home";
import theme from "./assets/theme/theme";
import Layout from "./hoc/Layout";
import RestorePassword from "./containers/Session/RestorePassword";
import ConfirmToken from "./containers/Session/ConfirmToken";
import { AuthContext } from "./context/auth-context";
import CandidateProfile from "./containers/Candidates/CandidateProfile";
import ListProfile from "./containers/Candidates/ListProfile";
import ListRegister from "./containers/Election/CreateCandidatesList";
import Profile from "./containers/Session/Profile";
import CandidatesListSearch from "./containers/Candidates/CandidatesListsSearch";
import CandidateDetails from "./containers/Candidates/CandidateDetails";
import ListDetails from "./containers/Candidates/ListDetails";
import Election from "./containers/Election/Election";
import ChangePassword from "./containers/Session/ChangePassword";
import Notifications from "./containers/Session/Notifications";
import Vote from "./containers/Election/Vote";
import ConfirmVote from "./containers/Election/ConfirmVote";
import FinishVote from "./containers/Election/FinishVote";
import Results from "./containers/Election/Results";

const App = () => {
  const authContext = React.useContext(AuthContext);

  let activeSession = true;
  const session = JSON.parse(localStorage.getItem("Fecha"));
  if (session) {
    //console.log("Tiempo de Sesion:");
    //console.log(Date.now() - session.fecha);
    if (Date.now() - session.fecha > 86400000) {
      activeSession = false;
    }
  }

  let routes = (
    <Switch>
      <Route path={ROUTES.LOGIN}>
        <Login />
      </Route>
      <Route path={ROUTES.RECUPERAR_CONTRASENA}>
        <RestorePassword />
      </Route>
      <Route path={ROUTES.CONFIRMAR_TOKEN}>
        <ConfirmToken />
      </Route>
      <Route path={ROUTES.CAMBIAR_CONTRASENA}>
        <ChangePassword />
      </Route>
      <Redirect from="/" to={ROUTES.LOGIN} />
    </Switch>
  );

  if (authContext.isAuth && activeSession) {
    routes = (
      <Layout TitleLayout="Inicio">
        <Switch>
          <Route exact path={ROUTES.CANDIDATO_DETALLES}>
            <CandidateDetails />
          </Route>
          <Route exact path={ROUTES.LISTA_DETALLES}>
            <ListDetails />
          </Route>
          <Route exact path={ROUTES.CANDIDATOS}>
            <CandidatesListSearch />
          </Route>
          <Route exact path={ROUTES.RESULTADOS}>
            <Results />
          </Route>
          <Route exact path={ROUTES.CONSTANCIA_VOTO}>
            <FinishVote />
          </Route>
          <Route exact path={ROUTES.CONFIRMAR_VOTACION}>
            <ConfirmVote />
          </Route>
          <Route exact path={ROUTES.VOTACION}>
            <Vote />
          </Route>
          <Route exact path={ROUTES.REGISTRO_LISTA}>
            <ListRegister />
          </Route>
          <Route exact path={ROUTES.ELECCION}>
            <Election />
          </Route>
          <Route exact path={ROUTES.NOTIFICACIONES}>
            <Notifications />
          </Route>
          <Route exact path={ROUTES.PERFIL_LISTA_CANDIDATO}>
            <ListProfile />
          </Route>
          <Route exact path={ROUTES.PERFIL_CANDIDATO}>
            <CandidateProfile />
          </Route>
          <Route exact path={ROUTES.PERFIL}>
            <Profile />
          </Route>
          <Route path={ROUTES.RECUPERAR_CONTRASENA}>
            <RestorePassword />
          </Route>
          <Route exact path={ROUTES.INICIO}>
            <HomeComponent />
          </Route>
          <Redirect from="/" to={ROUTES.INICIO} />
        </Switch>
      </Layout>
    );
  }

  return (
    <ThemeProvider theme={theme}>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <BrowserRouter>{routes}</BrowserRouter>
      </MuiPickersUtilsProvider>
    </ThemeProvider>
  );
};

export default App;
